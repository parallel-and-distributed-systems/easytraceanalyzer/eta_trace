#pragma once

#include <eta/core/debug.hpp>
#include <eta/core/filesystem.hpp>
#include <ostream>
#include <string>

namespace eta {

struct [[nodiscard]] ParsingError final {
    std::string what;
    std::string src;
    eta::Path file;
    std::size_t line;
    std::size_t column;
    std::size_t length;

    ParsingError(std::string w, std::string_view s, std::size_t c, std::size_t g)
            : what(std::move(w))
            , src(s)
            , file("")
            , line(0u)
            , column(c)
            , length(g) {}
    ParsingError(std::string w)
            : what(std::move(w))
            , src("")
            , file("")
            , line(0u)
            , column(0u)
            , length(0u) {}
};

auto operator<<(std::ostream &, ParsingError const &) -> std::ostream &;

}  // namespace eta
