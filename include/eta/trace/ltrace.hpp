#pragma once

#include <eta/core/filesystem.hpp>

#include "definitions.hpp"
#include "parsing_error.hpp"

namespace eta::ltrace {

#ifdef ENABLE_LTRACE

auto accept(Path const &) -> bool;

auto loadTrace(Path const & path) -> std::variant<Trace, ParsingError>;

#endif

}  // namespace eta::ltrace
