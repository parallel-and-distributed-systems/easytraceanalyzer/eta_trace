#pragma once

#include <cstdint>
#include <map>

#include "definitions.hpp"
#include "parsing_error.hpp"

namespace eta::paje {

#ifdef ENABLE_PAJE

enum class EventClass : std::uint8_t {
    // Types
    DefineContainerType,  // %PajeDefineContainerType Name Type [Alias]
    DefineStateType,      // %PajeDefineStateType Name Type [Alias]
    DefineEventType,      // %PajeDefineEventType
    DefineVariableType,   // %PajeDefineVariableType
    DefineLinkType,       // %PajeDefineLinkType
    DefineEntityValue,    // %PajeDefineEntityValue
    // Container
    CreateContainer,   // %PajeCreateContainer Time Name Type Container [Alias]
    DestroyContainer,  // %PajeDestroyContainer
    // States
    PushState,   // %PajePushState
    PopState,    // %PajePopState
    ResetState,  // %PajeResetState
    SetState,    // %PajeSetState
    // Event
    NewEvent,  // %PajeNewEvent
    // Variable
    SetVariable,  // %PajeSetVariable
    AddVariable,  // %PajeAddVariable
    SubVariable,  // %PajeSubVariable
    // Link
    StartLink,  // %PajeStartLnk
    EndLink,    // %PajeEndLink
};

auto to_string(EventClass) -> std::string_view;

struct EventTypeField {
    FieldId id;
    ValueType type;
};

struct EventType {
    EventClass event_class;
    std::vector<EventTypeField> fields;
};

struct EventTypeId_d final {
    using underlying_t = std::int16_t;
};
using EventTypeId = nlc::meta::id<EventTypeId_d>;

struct AdditionalData final : public TraceAdditionalData {
    std::map<EventTypeId, EventType> event_types;
    std::vector<EventTypeId> events_types_table;
};

auto accept(Path const &) -> bool;

auto loadTrace(Path const & path) -> std::variant<Trace, ParsingError>;

#endif

}  // namespace eta::paje
