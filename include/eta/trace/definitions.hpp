#pragma once

#ifdef ENABLE_OTF2
#    include <otf2/OTF2_Events.h>
#endif

#include <eta/core/debug.hpp>
#include <eta/core/filesystem.hpp>
#include <limits>
#include <nlc/meta/id.hpp>
#include <nlc/meta/units.hpp>
#include <optional>
#include <string>
#include <string_view>
#include <variant>
#include <vector>

namespace eta {

// ---------------------------------------------------

struct EventId_d final {
    using underlying_t = std::uint64_t;
    static constexpr auto invalid_value = std::numeric_limits<underlying_t>::max();
    static constexpr auto default_value = invalid_value;
};
using EventId = nlc::meta::id<EventId_d>;

struct ContainerId_d final {
    using underlying_t = std::uint32_t;
    static constexpr auto invalid_value = std::numeric_limits<underlying_t>::max();
    static constexpr auto default_value = invalid_value;
};
using ContainerId = nlc::meta::id<ContainerId_d>;

struct CommunicationId_d final {
    using underlying_t = std::uint64_t;
    static constexpr auto invalid_value = std::numeric_limits<underlying_t>::max();
    static constexpr auto default_value = invalid_value;
};
using CommunicationId = nlc::meta::id<CommunicationId_d>;

// ---------------------------------------------------

enum class ValueType {
    Color,
    Date,
    Double,
    Int,
    Uint32,
    Uint64,
    String,
    StringView,
    EventId,
    ContainerId,
    CommunicationId,
};

auto to_string(ValueType) -> std::string_view;
auto operator<<(std::ostream &, ValueType const &) -> std::ostream &;

// ---------------------------------------------------

struct Color final {
    unsigned char r, g, b;
};

using Date = nlc::meta::seconds<double>;

using Double = double;

using Int = long long int;
using Uint32 = uint32_t;
using Uint64 = uint64_t;

using String = std::string;

using StringView = std::string_view;

// ---------------------------------------------------

enum class ColorFormat {
    HTML,
    Paje,
    RGB,
    RGB_d,
};

auto to_string(Color const &, ColorFormat = ColorFormat::HTML) -> std::string;

// ---------------------------------------------------

using Value = std::variant<Color,
                           Date,
                           Double,
                           Int,
                           Uint32,
                           Uint64,
                           String,
                           StringView,
                           EventId,
                           ContainerId,
                           CommunicationId>;

auto to_string(Value const &) -> std::string;
auto operator<<(std::ostream &, Value const &) -> std::ostream &;
auto type_of(Value const &) -> ValueType;
auto operator<(Value const &, Value const &) -> bool;

auto operator<<(std::ostream &, Color const &) -> std::ostream &;
auto operator<<(std::ostream &, Date const &) -> std::ostream &;
auto operator<<(std::ostream &, EventId const &) -> std::ostream &;
auto operator<<(std::ostream &, ContainerId const &) -> std::ostream &;
auto operator<<(std::ostream &, CommunicationId const &) -> std::ostream &;

// ---------------------------------------------------

enum class EventType {
    CreateContainer,
    DestroyContainer,
    SendMessage,
    ReceiveMessage,
    EnterFunction,
    ExitFunction,
    IoOperationBegin,
    IoOperationComplete,
    MpiOperationBegin,
    MpiOperationComplete,
    Other,
};

auto to_string(EventType) -> std::string_view;
auto operator<<(std::ostream &, EventType) -> std::ostream &;

// ---------------------------------------------------

struct FieldId_d final {
    using underlying_t = std::uint16_t;
    static constexpr underlying_t invalid_value = std::numeric_limits<underlying_t>::max();
    static constexpr underlying_t default_value = invalid_value;
};
using FieldId = nlc::meta::id<FieldId_d>;

class FieldRegistry final {
  public:
    auto register_new_field_name(std::string) -> FieldId;
    auto get_field_name(FieldId) const -> std::string const &;
    auto get_field_id(std::string const &) const -> FieldId;
    decltype(auto) list() const { return (_field_names); }

  private:
    std::vector<std::string> _field_names;
};

// ----------------------------------------------------

struct StandardFieldIds {
    FieldId CommunicationDestination;
    FieldId CommunicationId;
    FieldId CommunicationLength;
    FieldId CommunicationSource;
    FieldId CommunicationTag;
    FieldId Container;
    FieldId EnterFunctionEvent;
    FieldId ExitFunctionEvent;
    FieldId FunctionName;
};

// ---------------------------------------------------

struct Field final {
    FieldId id;
    Value value;

    Field(FieldId i, Value v) : id(i), value(std::move(v)) { Assert(id.isValid(), ""); }
};

// ---------------------------------------------------

struct Event final {
    Date timestamp = Date(0);
    EventId id;
    std::vector<EventId> happensBefore;
    EventId happensAfter;
    EventType type;
    ContainerId container_id;
    std::vector<Field> fields;
};

auto get_field(Event const &, FieldId) -> std::optional<Value>;
auto operator<<(std::ostream & os, std::optional<Value> const & v) -> std::ostream &;
// ----------------------------------------------------

enum class CommunicationType {
    P2P,
    Barrier,
    BCast,
    Gather,
    Scatter,
    Reduce,
    Alltoall,
    Allgather,
    Reduce_scatter,
    Allreduce,
    Ibarrier,
    Ibcast,
    Igather,
    Iscatter,
    Ireduce,
    Ialltoall,
    Iallgather,
    Ireduce_scatter,
    Iallreduce,
    OMP,
    Other
};

auto to_string(CommunicationType) -> std::string_view;
auto operator<<(std::ostream &, CommunicationType const &) -> std::ostream &;

struct Communication final {
    ContainerId src;
    std::vector<ContainerId> dests;
    Int len;
    Int tag;
    std::vector<EventId> from;
    std::vector<EventId> to;
    CommunicationType type;
    // CommunicationId id;
};

// ----------------------------------------------------

struct Container final {
    std::string name;
    std::vector<EventId> events;
    ContainerId parent;
    ContainerId id;
    bool is_terminal;
    double offset;
};

// ----------------------------------------------------

struct TraceAdditionalData {
    virtual ~TraceAdditionalData() = default;
};

struct Trace final {
  public:
    Trace();
    Trace(Trace &&) = default;
    Trace(Trace const &) = delete;
    auto operator=(Trace &&) -> Trace & = default;
    auto operator=(Trace const &) -> Trace const & = delete;

  private:
    struct TraceInitializerData;
    Trace(TraceInitializerData &&);

  public:
    std::string name;
    eta::Path path;
    std::string type;

    std::vector<Event> events;
    std::vector<Container> containers;
    std::vector<Communication> communications;

    std::vector<EventId> events_without_container;

    StandardFieldIds field_ids;
    FieldRegistry fields;

    std::unique_ptr<TraceAdditionalData> additional_data;

    [[nodiscard]] decltype(auto) event(EventId id) const {
        Assert(id.isValid() && id.value() < events.size(),
               id.isValid(),
               " && ",
               id.value(),
               " < ",
               events.size());
        return events[id.value()];
    }
    [[nodiscard]] decltype(auto) event(EventId id) {
        Assert(id.isValid() && id.value() < events.size(),
               id.isValid(),
               " && ",
               id.value(),
               " < ",
               events.size());
        return events[id.value()];
    }

    auto get_field(Event const & e, std::string const & fieldName) const -> std::optional<Value> {
        eta::FieldId fId = fields.get_field_id(fieldName);
        return eta::get_field(e, fId);
    }

    [[nodiscard]] decltype(auto) communication(CommunicationId id) const {
        Assert(id.isValid() && id.value() < communications.size(),
               id.isValid(),
               " && ",
               id.value(),
               " < ",
               communications.size());
        return communications[id.value()];
    }
    [[nodiscard]] decltype(auto) communication(CommunicationId id) {
        Assert(id.isValid() && id.value() < communications.size(),
               id.isValid(),
               " && ",
               id.value(),
               " < ",
               communications.size());
        return communications[id.value()];
    }

    [[nodiscard]] decltype(auto) container(ContainerId id) const {
        Assert(id.isValid() && id.value() < containers.size(),
               id.isValid(),
               " && ",
               id.value(),
               " < ",
               containers.size());
        return containers[id.value()];
    }
    [[nodiscard]] decltype(auto) container(ContainerId id) {
        Assert(id.isValid() && id.value() < containers.size(),
               id.isValid(),
               " && ",
               id.value(),
               " < ",
               containers.size());
        return containers[id.value()];
    }

    [[nodiscard]] auto container(std::string_view container_name) -> eta::Container * {
        for (auto & c : containers) {
            if (c.name.compare(container_name) == 0)
                return &c;
        }
        return (eta::Container *)nullptr;
    }

    auto createContainer(std::string container_name,
                         ContainerId parentId = ContainerId::invalid(),
                         bool is_terminal = false) -> Container &;
    auto push_back_event(Event & event, ContainerId container_id = ContainerId::invalid()) -> void;
};

// ----------------------------------------------------

#ifdef ENABLE_OTF2
auto to_string(OTF2_CollectiveOp) -> std::string;
auto to_communication_type(OTF2_CollectiveOp) -> CommunicationType;
#endif

}  // namespace eta
