#pragma once

#include <eta/core/filesystem.hpp>
#include <variant>

#include "definitions.hpp"
#include "parsing_error.hpp"

namespace eta::otf2 {

#ifdef ENABLE_OTF2

auto accept(Path const &) -> bool;

auto loadTrace(Path const & path) -> std::variant<Trace, ParsingError>;

#endif

}  // namespace eta::otf2
