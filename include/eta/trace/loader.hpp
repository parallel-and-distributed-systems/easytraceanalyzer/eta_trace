#pragma once

#include <eta/core/filesystem.hpp>

#include "definitions.hpp"
#include "parsing_error.hpp"

namespace eta {

enum class TraceType {
    Paje,
    Otf2,
    LTrace,
    Darshan,
    Auto,
};

auto evaluateTraceType(Path const & path) -> TraceType;
auto loadTrace(Path const & path, TraceType type = TraceType::Auto)
        -> std::variant<Trace, ParsingError>;

}  // namespace eta
