#!/bin/bash

# Get otf2
if [ ! -d otf2-2.3 ]
then
    curl https://perftools.pages.jsc.fz-juelich.de/cicd/otf2/tags/otf2-2.3/otf2-2.3.tar.gz --output otf2-2.3.tar.gz
    tar -xf otf2-2.3.tar.gz
    rm otf2-2.3.tar.gz
    cd otf2-2.3
    ./configure --prefix=`pwd`/install || exit 1
    cd ..
fi

# Configure and build otf2
make -C otf2-2.3 install

