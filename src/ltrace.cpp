#include "ltrace.hpp"

namespace eta::ltrace {

auto accept(Path const & path) -> bool { return path.extension() == "ltrace"; }

auto loadTrace(Path const & path) -> std::variant<Trace, ParsingError> {
    auto err = ParsingError { "LTrace is not implemented yet" };
    err.file = path;
    return err;
}

}  // namespace eta::ltrace
