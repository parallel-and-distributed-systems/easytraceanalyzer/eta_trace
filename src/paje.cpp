#include "paje.hpp"

#include <bits/c++config.h>

#include <algorithm>
#include <cstdint>
#include <cstring>
#include <eta/core/debug.hpp>
#include <eta/core/filesystem.hpp>
#include <fstream>
#include <list>
#include <map>
#include <nlc/meta/overloaded.hpp>
#include <optional>
#include <type_traits>
#include <variant>

#include "definitions.hpp"
#include "error_management.hpp"
#include "parsing.hpp"
#include "parsing_error.hpp"

namespace eta::paje {

auto to_string(EventClass ec) -> std::string_view {
    switch (ec) {
        case EventClass::DefineContainerType: return "PajeDefineContainerType";
        case EventClass::DefineStateType: return "PajeDefineStateType";
        case EventClass::DefineEventType: return "PajeDefineEventType";
        case EventClass::DefineVariableType: return "PajeDefineVariableType";
        case EventClass::DefineLinkType: return "PajeDefineLinkType";
        case EventClass::DefineEntityValue: return "PajeDefineEntityValue";
        case EventClass::CreateContainer: return "PajeCreateContainer";
        case EventClass::DestroyContainer: return "PajeDestroyContainer";
        case EventClass::PushState: return "PajePushState";
        case EventClass::PopState: return "PajePopState";
        case EventClass::ResetState: return "PajeResetState";
        case EventClass::SetState: return "PajeSetState";
        case EventClass::NewEvent: return "PajeNewEvent";
        case EventClass::SetVariable: return "PajeSetVariable";
        case EventClass::AddVariable: return "PajeAddVariable";
        case EventClass::SubVariable: return "PajeSubVariable";
        case EventClass::StartLink: return "PajeStartLnk";
        case EventClass::EndLink: return "PajeEndLink";
        default: Assert(false, "Unknown paje event class"); return "UNKNOWN";
    }
}

template <typename T, typename... R> static auto isIn(T && v, R &&... args) -> bool {
    return ((nlc_fwd(v) == nlc_fwd(args)) || ...);
}

#define CHECK_SINGLE_CASE(name)                                                         \
    do {                                                                                \
        constexpr auto str_size = static_cast<std::size_t>(sizeof(#name) - 1);          \
        if (remaining >= str_size && std::strncmp(&src[index], #name, str_size) == 0) { \
            begin = index + str_size;                                                   \
            return EventClass::name;                                                    \
        }                                                                               \
    } while (false)

#define CHECK_DEFINE_CASE(name)                                                           \
    do {                                                                                  \
        constexpr auto s = static_cast<std::size_t>(sizeof(#name) - 1); /* remove '\0` */ \
        if (r >= sizeof(#name) && std::strncmp(&src[i], #name, s) == 0) {                 \
            begin = i + s;                                                                \
            return EventClass::Define##name;                                              \
        }                                                                                 \
    } while (false)

[[nodiscard]] static auto consumeEventClass(std::string_view const & src, std::size_t & begin)
        -> std::variant<EventClass, ParsingError> {
    if (src.size() <= begin)
        return ParsingError { "Unexpected end of line", src, begin, 0 };

    constexpr auto paje_size = static_cast<std::size_t>(sizeof("Paje") - 1);  // remove '\0'
    static_assert(paje_size == 4);
    if (src.size() - begin >= paje_size && std::strncmp(&src[begin], "Paje", paje_size) == 0) {
        auto const index = begin + paje_size;
        auto const remaining = src.size() - index;
        switch (src[index]) {
            case 'A': CHECK_SINGLE_CASE(AddVariable); break;
            case 'C': CHECK_SINGLE_CASE(CreateContainer); break;
            case 'D': {
                constexpr auto def_size = static_cast<std::size_t>(sizeof("Define") - 1);  // remove
                                                                                           // '\0'
                if (remaining > def_size &&
                    std::strncmp(src.begin() + index, "Define", def_size) == 0) {
                    auto const i = index + def_size;
                    auto const r = src.size() - i;
                    CHECK_DEFINE_CASE(ContainerType);
                    CHECK_DEFINE_CASE(EntityValue);
                    CHECK_DEFINE_CASE(EventType);
                    CHECK_DEFINE_CASE(LinkType);
                    CHECK_DEFINE_CASE(StateType);
                    CHECK_DEFINE_CASE(VariableType);
                }
                CHECK_SINGLE_CASE(DestroyContainer);
            } break;
            case 'E': CHECK_SINGLE_CASE(EndLink); break;
            case 'N': CHECK_SINGLE_CASE(NewEvent); break;
            case 'P':
                CHECK_SINGLE_CASE(PopState);
                CHECK_SINGLE_CASE(PushState);
                break;
            case 'R': CHECK_SINGLE_CASE(ResetState); break;
            case 'S':
                CHECK_SINGLE_CASE(SetState);
                CHECK_SINGLE_CASE(SetVariable);
                CHECK_SINGLE_CASE(StartLink);
                CHECK_SINGLE_CASE(SubVariable);
                break;
        }
    }
    auto i = begin;
    consumeToken(src, i);
    return ParsingError { eta::format(src.substr(begin, i - begin),
                                      " does not name a paje event class"),
                          src,
                          begin,
                          i - begin };
}
#undef CHECK_DEFINE_CASE
#undef CHECK_SINGLE_CASE

#define CHECK_SINGLE_CASE(name, value)                                                   \
    {                                                                                    \
        auto const str_size = static_cast<std::size_t>(sizeof(name) - 1);                \
        if (remaining >= str_size && (std::strncmp(&src[begin], name, str_size) == 0)) { \
            begin += str_size;                                                           \
            return ValueType::value;                                                     \
        }                                                                                \
    }

[[nodiscard]] static auto consumeValueType(std::string_view const & src, std::size_t & begin)
        -> std::variant<ParsingError, ValueType> {
    if (src.size() == begin)
        return ParsingError { "Unexpected end of line", src, begin, 0 };
    auto const remaining = src.size() - begin;
    switch (src[begin]) {
        case 'c': CHECK_SINGLE_CASE("color", Color); break;
        case 'd':
            CHECK_SINGLE_CASE("date", Date);
            CHECK_SINGLE_CASE("double", Double);
            break;
        case 'h': CHECK_SINGLE_CASE("hex", String); break;
        case 'i': CHECK_SINGLE_CASE("int", Int); break;
        case 's': CHECK_SINGLE_CASE("string", String); break;
    }
    auto i = begin;
    consumeToken(src, i);
    return ParsingError {
        eta::format("Unknown paje data type \"", src.substr(begin, i - begin), '"'),
        src,
        begin,
        i - begin
    };
}

#undef CHECK_SINGLE_CASE

[[nodiscard]] static auto consumeDate(std::string_view const & str, std::size_t & begin)
        -> std::variant<Date, ParsingError> {
    errno = 0;
    auto it = static_cast<char const *>(str.begin() + begin);
    auto const res = Date(std::strtod(it, const_cast<char **>(&it)));
    auto const advance = static_cast<std::size_t>(it - str.begin());
    if (errno != 0 || advance <= 0) {
        errno = 0;
        return ParsingError { "Failed to parse double", str, begin, 0 };
    }
    begin = advance;
    return res;
}

[[nodiscard]] static auto consumeColor(std::string_view const & str, std::size_t & begin)
        -> std::variant<Color, ParsingError> {
    auto const err1 = consume(str, begin, "\"");
    FORWARD_SIMPLE_ERROR(err1);

    auto color = Color {};

#define GET_FIELD(v)                                                 \
    {                                                                \
        auto const tmp = consumeDouble(str, begin);                  \
        FORWARD_ERROR(tmp);                                          \
        v = static_cast<std::uint8_t>(std::get<double>(tmp) * 255.); \
    }

    GET_FIELD(color.r);
    consumeSpaces(str, begin);
    GET_FIELD(color.g);
    consumeSpaces(str, begin);
    GET_FIELD(color.b);

#undef GET_FIELD

    auto const err2 = consume(str, begin, "\"");
    FORWARD_SIMPLE_ERROR(err2);

    return color;
}

enum class ParsingState {
    InHeader,
    InEventDef,
    InBody,
};

struct PajeFieldIds final {
    FieldId Alias;
    FieldId Key;
    FieldId Name;
    FieldId PajeClass;
    FieldId Time;
    FieldId Type;
    FieldId Value;
    // to delete
    FieldId StartContainer;
    FieldId EndContainer;
    // FieldId Container;
};

static auto registerPajeFieldIds(PajeFieldIds & ids, Trace & trace) {
#define IMPL(name) ids.name = trace.fields.register_new_field_name(#name)
    IMPL(Alias);
    IMPL(Key);
    IMPL(Name);
    IMPL(PajeClass);
    IMPL(Time);
    IMPL(Type);
    IMPL(Value);

    IMPL(StartContainer);
    IMPL(EndContainer);
    // IMPL(Container);
#undef IMPL
}

#undef IMPL

struct State {
    EventId enter_function_event;
    Value function_name;
};

struct Message {
    int sender;
    int receiver;
    int len;
    int tag;
    EventId event_send;
    EventId event_receive;
    CommunicationType type;
};

struct TemporaryData final {
    PajeFieldIds field_ids;
    std::map<std::string, FieldId> field_names;
    EventType * current_event_type = nullptr;
    std::map<Value, ContainerId> containers_by_name;
    std::map<std::pair<ContainerId, Value>, std::vector<State>> states;

    // used to match messages
    std::map<std::string, Message> msg_sent_by_key;
    std::map<std::string, Message> msg_recv_by_key;

    // used to match OpenMP messages
    std::map<std::string, EventId> msg_end_keys;

    // used to match collective communications
    std::map<std::string, Communication> collective_comm_by_key;

    Message current_msg;
};

static auto get_or_create_field(TemporaryData * temporary_data,
                                Trace & trace,
                                std::string_view const & field_name) -> FieldId {
    auto id = trace.fields.get_field_id(std::string(field_name));

    if (id.isInvalid()) {
        auto [field_id_it, inserted] =
                temporary_data->field_names.emplace(field_name, FieldId::invalid());
        Assert(inserted, "");
        id = trace.fields.register_new_field_name(std::string(field_name));
    }

    return id;
}

static auto parseHeaderFirstLine(std::string_view line,
                                 std::size_t & index,
                                 AdditionalData * additional_data,
                                 TemporaryData * temporary_data) -> std::optional<ParsingError> {
    ++index;
    consumeSpaces(line, index);
    auto event_def = consume(line, index, "EventDef");
    FORWARD_SIMPLE_ERROR(event_def);
    consumeSpaces(line, index);
    auto paje_event_class = consumeEventClass(line, index);
    FORWARD_ERROR(paje_event_class);
    consumeSpaces(line, index);
    auto paje_event_index = consumeInt(line, index);
    FORWARD_ERROR(paje_event_index);
    CHECK_LINE_ENDED(line, index);
    auto [it, inserted] = additional_data->event_types.emplace(
            std::get<Int>(paje_event_index),
            EventType { std::get<EventClass>(paje_event_class), {} });
    if (!inserted)
        return ParsingError {
            eta::format("Event type ", std::get<Int>(paje_event_index), " was already declared"),
            line,
            0,
            line.size()
        };
    temporary_data->current_event_type = &it->second;
    return std::nullopt;
}

static auto parseEventDefLine(std::string_view line,
                              std::size_t & index,
                              Trace & trace,
                              TemporaryData * temporary_data)
        -> std::variant<ParsingState, ParsingError> {
    consumeSpaces(line, index);
    if (line[index] != '%')
        return ParsingError { "Unexpected char at begining of header line",
                              line,
                              index,
                              index + 1 };
    ++index;
    consumeSpaces(line, index);
    auto i = index;
    while (i != line.size() && (isDigit(line[i]) || isLetter(line[i])))
        ++i;
    auto const field_name = std::string_view(&line[index], i - index);
    if (field_name == "EndEventDef") {
        temporary_data->current_event_type = nullptr;
        return ParsingState::InHeader;
    } else {
        consumeSpaces(line, i);
        auto field_type = consumeValueType(line, i);
        FORWARD_ERROR(field_type);
        CHECK_LINE_ENDED(line, i);
        Assert(temporary_data->current_event_type != nullptr, "");
        auto const field_id = get_or_create_field(temporary_data, trace, field_name);
        temporary_data->current_event_type->fields.emplace_back(
                EventTypeField { field_id, std::get<ValueType>(field_type) });
    }
    CHECK_LINE_ENDED(line, i);
    return ParsingState::InEventDef;
}

static auto consumeValue(std::string_view line, std::size_t & index, ValueType type)
        -> std::variant<Value, ParsingError> {
    switch (type) {
        case ValueType::Color: {
            auto const v = consumeColor(line, index);
            FORWARD_ERROR(v);
            return Value(std::get<Color>(std::move(v)));
        }
        case ValueType::Date: {
            auto const v = consumeDate(line, index);
            FORWARD_ERROR(v);
            return Value(std::get<Date>(std::move(v)));
        }
        case ValueType::Double: {
            auto const v = consumeDouble(line, index);
            FORWARD_ERROR(v);
            return Value(std::get<Double>(std::move(v)));
        }
        case ValueType::Int: {
            auto const v = consumeInt(line, index);
            FORWARD_ERROR(v);
            return Value(std::get<Int>(std::move(v)));
        }
        case ValueType::String: {
            auto const v = consumeString(line, index);
            FORWARD_ERROR(v);
            return Value(std::get<String>(std::move(v)));
        }
        default:
            return ParsingError {
                eta::format("ValueType \"", to_string(type), "\" is not handled by paje"),
                line,
                index,
                to_string(type).size()
            };
    }
}

static auto getContainerId(Value const & v, TemporaryData const * temporary_data) -> ContainerId {
    auto const name = std::visit(
            nlc::meta::overloaded { [](String const & s) -> String { return s; },
                                    [](Int const & i) -> String { return std::to_string(i); },
                                    [](auto const &) -> String {
                                        Assert(false, "Container name must be a string or an int");
                                        return "";
                                    } },
            v);
    auto const it = temporary_data->containers_by_name.find(name);
    if (it != temporary_data->containers_by_name.end())
        return it->second;
    return ContainerId::invalid();
}

static auto getCommunicationType(std::string const & function_name) {
    if (function_name == "STV_MPI_BCast" || function_name == "MPI_BCast" ||
        function_name == "mpi_bcast_") {
        return CommunicationType::BCast;
    } else if (function_name == "STV_MPI_Barrier" || function_name == "MPI_Barrier" ||
               function_name == "mpi_barrier_") {
        return CommunicationType::Barrier;
    } else if (function_name == "STV_MPI_Gather" || function_name == "MPI_Gather" ||
               function_name == "mpi_gather_") {
        return CommunicationType::Gather;
    } else if (function_name == "STV_MPI_Scatter" || function_name == "MPI_Scatter" ||
               function_name == "mpi_scatter_") {
        return CommunicationType::Scatter;
    } else if (function_name == "STV_MPI_Reduce" || function_name == "MPI_Reduce" ||
               function_name == "mpi_reduce_") {
        return CommunicationType::Reduce;
    } else if (function_name == "STV_MPI_Alltoall" || function_name == "MPI_Alltoall" ||
               function_name == "mpi_alltoall_") {
        return CommunicationType::Alltoall;
    } else if (function_name == "STV_MPI_Allgather" || function_name == "MPI_Allgather" ||
               function_name == "mpi_allgather_") {
        return CommunicationType::Allgather;
    } else if (function_name == "STV_MPI_Reduce_scatter" || function_name == "MPI_Reduce_scatter" ||
               function_name == "mpi_reduce_scatter_") {
        return CommunicationType::Reduce_scatter;
    } else if (function_name == "STV_MPI_Allreduce" || function_name == "MPI_Allreduce" ||
               function_name == "mpi_allreduce_") {
        return CommunicationType::Allreduce;
    } else if (function_name == "STV_MPI_Ibcast" || function_name == "MPI_Ibcast" ||
               function_name == "mpi_ibcast_") {
        return CommunicationType::Ibcast;
    } else if (function_name == "STV_MPI_Ibarrier" || function_name == "MPI_Ibarrier" ||
               function_name == "mpi_ibarrier_") {
        return CommunicationType::Ibarrier;
    } else if (function_name == "STV_MPI_Igather" || function_name == "MPI_Igather" ||
               function_name == "mpi_igather_") {
        return CommunicationType::Igather;
    } else if (function_name == "STV_MPI_Iscatter" || function_name == "MPI_Iscatter" ||
               function_name == "mpi_iscatter_") {
        return CommunicationType::Iscatter;
    } else if (function_name == "STV_MPI_Ireduce" || function_name == "MPI_Ireduce" ||
               function_name == "mpi_ireduce_") {
        return CommunicationType::Ireduce;
    } else if (function_name == "STV_MPI_Ialltoall" || function_name == "MPI_Ialltoall" ||
               function_name == "mpi_ialltoall_") {
        return CommunicationType::Ialltoall;
    } else if (function_name == "STV_MPI_Iallgather" || function_name == "MPI_Iallgather" ||
               function_name == "mpi_iallgather_") {
        return CommunicationType::Iallgather;
    } else if (function_name == "STV_MPI_Ireduce_scatter" ||
               function_name == "MPI_Ireduce_scatter" || function_name == "mpi_ireduce_scatter_") {
        return CommunicationType::Ireduce_scatter;
    } else if (function_name == "STV_MPI_Iallreduce" || function_name == "MPI_Iallreduce" ||
               function_name == "mpi_iallreduce_") {
        return CommunicationType::Iallreduce;
    } else {
        std::cout << "Not recognized: " << function_name << "\n";
        return CommunicationType::Other;
    }
}

static auto consumeLinkEvent(std::vector<Field> fields,
                             Trace & trace,
                             Event & event,
                             EventType const & event_type,
                             TemporaryData * temporary_data) -> std::optional<ParsingError> {
    Assert(isIn(event_type.event_class, EventClass::StartLink, EventClass::EndLink), "");

    switch (event_type.event_class) {
        case EventClass::StartLink: event.type = eta::EventType::SendMessage; break;
        case EventClass::EndLink: event.type = eta::EventType::ReceiveMessage; break;
        default:
            Assert(false,
                   "Paje event class must be PajeStartLink or PajeEndLink but is ",
                   to_string(event_type.event_class));
            break;
    }

    std::string key = "";
    std::string comm_type = "";
    temporary_data->current_msg.len = 0;
    temporary_data->current_msg.tag = 0;

    for (auto & field : fields) {
        if (field.id == temporary_data->field_ids.Value) {
            int src;
            int dest;
            int len;
            int tag;

            Assert(isIn(type_of(field.value), ValueType::String, ValueType::StringView),
                   "Value must be a string, but is a ",
                   to_string(type_of(field.value)));
            auto const value_str = to_string(field.value);

            int ret = sscanf(value_str.c_str(),
                             "\"src=%d, dest=%d, len=%d, tag=%d\"",
                             &src,
                             &dest,
                             &len,
                             &tag);
            // Assert(ret == 4, "Failed to parse value field in link");

            // TODO: calling get_or_create_field may kill the performance !
            switch (ret) {
                case 4:
                    event.fields.emplace_back(
                            Field { get_or_create_field(temporary_data, trace, "tag"), Int(tag) });
                    temporary_data->current_msg.tag = tag;
                    [[fallthrough]];
                case 3:
                    event.fields.emplace_back(
                            Field { get_or_create_field(temporary_data, trace, "len"), Int(len) });
                    temporary_data->current_msg.len = len;
                    [[fallthrough]];
                case 2:
                    event.fields.emplace_back(
                            Field { get_or_create_field(temporary_data, trace, "dest"),
                                    Int(dest) });
                    temporary_data->current_msg.receiver = dest;
                    [[fallthrough]];
                case 1:
                    event.fields.emplace_back(
                            Field { get_or_create_field(temporary_data, trace, "src"), Int(src) });
                    temporary_data->current_msg.sender = src;
                    break;
                default:
                    event.fields.emplace_back(
                            Field { temporary_data->field_ids.Value, field.value });
            }

        } else if (field.id == temporary_data->field_ids.StartContainer &&
                   event_type.event_class == EventClass::StartLink) {
            event.container_id = getContainerId(field.value, temporary_data);
            if (!event.container_id.isValid())
                return ParsingError { eta::format("Invalid container identifier ",
                                                  to_string(field.value)) };
            event.fields.emplace_back(Field { field.id, event.container_id });
        } else if (field.id == temporary_data->field_ids.EndContainer &&
                   event_type.event_class == EventClass::EndLink) {
            event.container_id = getContainerId(field.value, temporary_data);
            if (!event.container_id.isValid())
                return ParsingError { eta::format("Invalid container identifier ",
                                                  to_string(field.value)) };
            event.fields.emplace_back(Field { field.id, event.container_id });
        } else if (field.id == temporary_data->field_ids.Key) {
            key = std::get<String>(field.value);
            event.fields.emplace_back(Field { field.id, std::move(field.value) });
        } else if (field.id == temporary_data->field_ids.Type) {
            comm_type = std::get<std::string>(field.value);
            event.fields.emplace_back(Field { field.id, std::move(field.value) });
        } else {
            event.fields.push_back(std::move(field));
        }
    }

    if (comm_type == "L_MPI_P2P") {
        temporary_data->current_msg.type = CommunicationType::P2P;
    } else if (comm_type == "GOMP_Section_Link") {
        temporary_data->current_msg.type = CommunicationType::OMP;
    } else {
        auto collective_key = key.substr(key.find("0x"));
        temporary_data->current_msg.type = CommunicationType::Other;

        if (temporary_data->collective_comm_by_key.count(collective_key) == 0) {
            temporary_data->collective_comm_by_key.emplace(
                    collective_key,
                    Communication { ContainerId::invalid(),               // src
                                    { event.container_id },               // dests
                                    temporary_data->current_msg.len,      // len
                                    temporary_data->current_msg.tag,      // tag
                                    {},                                   // from
                                    {},                                   // to
                                    temporary_data->current_msg.type });  // type

            if (event.type == eta::EventType::SendMessage) {
                temporary_data->collective_comm_by_key[collective_key].from.emplace_back(event.id);
            } else {
                temporary_data->collective_comm_by_key[collective_key].to.emplace_back(event.id);
            }
        } else {
            auto & comm = temporary_data->collective_comm_by_key[collective_key];

            if (event.type == eta::EventType::SendMessage) {
                if (std::count(comm.dests.begin(), comm.dests.end(), event.container_id) == 0) {
                    comm.dests.emplace_back(event.container_id);
                }

                if (comm.type == CommunicationType::Other) {
                    auto const & enter_event =
                            trace.event(trace.event(comm.from.back()).happensAfter);
                    auto const f_name = std::get<eta::String>(
                            get_field(enter_event, trace.field_ids.FunctionName).value());

                    comm.type = getCommunicationType(f_name);
                }

                comm.from.emplace_back(event.id);
            } else {
                comm.to.emplace_back(event.id);
            }
        }
    }

    if (event.type == eta::EventType::SendMessage) {
        auto it_enter = *std::find_if(trace.events.rbegin(),
                                      trace.events.rend(),
                                      [event](eta::Event const & e) {
                                          return e.container_id == event.container_id &&
                                                 e.type == eta::EventType::EnterFunction;
                                      });

        trace.event(it_enter.id).happensBefore.emplace_back(event.id);
        event.happensAfter = it_enter.id;

        temporary_data->current_msg.event_send = event.id;
        auto it = temporary_data->msg_recv_by_key.empty()
                          ? temporary_data->msg_recv_by_key.end()
                          : temporary_data->msg_recv_by_key.find(key);

        if (it != temporary_data->msg_recv_by_key.end()) {
            // a receive event for this message has already been processed
            auto const & msg = (*it).second;

            if (msg.type == eta::CommunicationType::P2P ||
                msg.type == eta::CommunicationType::OMP) {
                trace.communications.emplace_back(
                        Communication { event.container_id,                               // src
                                        { trace.event(msg.event_receive).container_id },  // dests
                                        std::move(msg.len),                               // len
                                        std::move(msg.tag),                               // tag
                                        { event.id },                                     // from
                                        { msg.event_receive },                            // to
                                        std::move(msg.type) });                           // type
            }

            event.happensBefore.emplace_back(msg.event_receive);
            trace.event(msg.event_receive).happensAfter = event.id;

            temporary_data->msg_recv_by_key.erase(it);
        } else {
            temporary_data->msg_sent_by_key.emplace(key, temporary_data->current_msg);
        }

        // get the OpenMP key to match the Recv event for the fork with the Send event for the join
        if (comm_type == "GOMP_Section_Link" && key.find("_end") != std::string::npos) {
            auto it_end = temporary_data->msg_end_keys.find(key);

            if (it_end != temporary_data->msg_end_keys.end()) {
                trace.event(it_end->second).happensBefore.emplace_back(event.id);
                event.happensAfter = it_end->second;
                temporary_data->msg_end_keys.erase(it_end);
            } else {
                std::cout << "Warning: OMP join before fork is complete! Key: " << key << "\n";
            }
        }
    } else {
        temporary_data->current_msg.event_receive = event.id;
        auto it = temporary_data->msg_sent_by_key.empty()
                          ? temporary_data->msg_sent_by_key.end()
                          : temporary_data->msg_sent_by_key.find(key);

        if (it != temporary_data->msg_sent_by_key.end()) {
            // the send event for this message has already been processed
            auto const & msg = (*it).second;

            if (msg.type == eta::CommunicationType::P2P ||
                msg.type == eta::CommunicationType::OMP) {
                trace.communications.emplace_back(
                        Communication { trace.event(msg.event_send).container_id,  // src
                                        { event.container_id },                    // dests
                                        std::move(msg.len),                        // len
                                        std::move(msg.tag),                        // tag
                                        { msg.event_send },                        // from
                                        { event.id },                              // to
                                        std::move(msg.type) });                    // type
            }

            event.happensAfter = msg.event_send;
            trace.event(msg.event_send).happensBefore.emplace_back(event.id);

            temporary_data->msg_sent_by_key.erase(it);
        } else {
            // the send event has not happened yet...
            std::cout << "WARNING: a message has potentially been received "
                         "before it has been sent in the trace record! Key: "
                      << key << "\n";

            temporary_data->msg_recv_by_key.emplace(key, temporary_data->current_msg);
        }

        // register OpenMP message key to match the join with the fork
        if (comm_type == "GOMP_Section_Link" && key.find("_end") == std::string::npos) {
            temporary_data->msg_end_keys.emplace(key + "_end", event.id);
        }
    }

    return std::nullopt;
}

static auto consumeStateEvent(std::vector<Field> fields,
                              Trace & trace,
                              Event & event,
                              EventType const & event_type,
                              TemporaryData * temporary_data) -> std::optional<ParsingError> {
    Assert(isIn(event_type.event_class,
                EventClass::PopState,
                EventClass::PushState,
                EventClass::ResetState,
                EventClass::SetState),
           "");
    Assert(!event.container_id.isValid(),
           "Paje event class must be PajeStartLink or PajeEndLink but is ",
           to_string(event_type.event_class));

    auto value = std::optional<eta::Value>(std::nullopt);
    auto type = std::optional<eta::Value>(std::nullopt);

    for (auto & field : fields) {
        if (field.id == trace.field_ids.Container) {
            event.container_id = getContainerId(field.value, temporary_data);
            if (!event.container_id.isValid())
                return ParsingError { eta::format("Invalid container identifier ",
                                                  to_string(field.value)) };
            event.fields.emplace_back(Field { field.id, event.container_id });
        } else if (field.id == temporary_data->field_ids.Type) {
            type = std::move(field.value);
        } else if (field.id == temporary_data->field_ids.Value) {
            value = std::move(field.value);
        } else {
            event.fields.emplace_back(std::move(field));
        }
    }

    if (!event.container_id.isValid()) {
        return ParsingError { eta::format("Missing \"Container\" field in ",
                                          to_string(event_type.event_class),
                                          " Paje event") };
    }
    if (type == std::nullopt) {
        return ParsingError { eta::format("Missing \"Type\" field in ",
                                          to_string(event_type.event_class),
                                          " Paje event") };
    }

    auto const it = temporary_data->states.emplace(std::pair { event.container_id, *type },
                                                   std::vector<State> {});
    auto & states = it.first->second;

    event.fields.emplace_back(Field { temporary_data->field_ids.Type, std::move(*type) });

    switch (event_type.event_class) {
        case EventClass::PopState: {
            event.type = eta::EventType::ExitFunction;
            if (states.size() > 0) {
                auto const enter_function_event_id = states.back().enter_function_event;
                auto & enter_function_event = trace.event(enter_function_event_id);
                event.fields.emplace_back(Field { trace.field_ids.FunctionName,
                                                  std::move(states.back().function_name) });
                event.fields.emplace_back(
                        Field { trace.field_ids.EnterFunctionEvent, enter_function_event_id });
                enter_function_event.fields.emplace_back(
                        Field { trace.field_ids.ExitFunctionEvent, event.id });
                states.pop_back();
            } else
                log_warning("Tried to pop state in empty stape stack");
            if (value != std::nullopt)
                event.fields.emplace_back(
                        Field { temporary_data->field_ids.Value, std::move(*value) });
        } break;
        case EventClass::PushState: {
            event.type = eta::EventType::EnterFunction;
            if (value != std::nullopt) {
                event.fields.emplace_back(Field { trace.field_ids.FunctionName, *value });
                states.push_back(State { event.id, std::move(*value) });
            } else
                return ParsingError { eta::format("Missing \"Value\" field in ",
                                                  to_string(event_type.event_class),
                                                  " Paje event") };
        } break;
        case EventClass::ResetState: {
            event.type = eta::EventType::Other;
            if (value != std::nullopt) {
                states.clear();
                states.push_back(State { event.id, std::move(*value) });
                event.fields.emplace_back(
                        Field { temporary_data->field_ids.Value, std::move(*value) });
            } else
                return ParsingError { eta::format("Missing \"Value\" field in ",
                                                  to_string(event_type.event_class),
                                                  " Paje event") };
        } break;
        case EventClass::SetState: {
            event.type = eta::EventType::Other;
            if (value != std::nullopt) {
                event.fields.emplace_back(
                        Field { temporary_data->field_ids.Value, std::move(*value) });
                if (!states.empty())
                    states.pop_back();
                states.push_back(State { event.id, std::move(*value) });
            } else
                return ParsingError { eta::format("Missing \"Value\" field in ",
                                                  to_string(event_type.event_class),
                                                  " Paje event") };
        } break;
        default: break;
    }

    return std::nullopt;
}

static auto consumeCreateContainerEvent(std::vector<Field> fields,
                                        Trace & trace,
                                        Event & event,
                                        EventType const & /*event_type*/,
                                        TemporaryData * temporary_data)
        -> std::optional<ParsingError> {
    // Assert(event_type.event_class == EventClass::CreateContainer, "");

    event.container_id =
            ContainerId(static_cast<ContainerId::underlying_t>(trace.containers.size()));
    Assert(event.container_id.isValid() && event.container_id.value() == trace.containers.size(),
           "Container count exceed ContainerId capacity");
    trace.containers.emplace_back();
    auto & container = trace.container(event.container_id);
    container.id = event.container_id;
    container.is_terminal = true;

    for (auto & field : fields) {
        if (field.id == trace.field_ids.Container) {
            auto const it = temporary_data->containers_by_name.find(field.value);
            if (it == temporary_data->containers_by_name.end())
                return ParsingError {
                    eta::format("Container \"", field.value, "\" was not declared")
                };
            event.fields.emplace_back(Field { field.id, event.container_id });
            container.parent = it->second;
            if (container.parent.isValid())
                trace.container(container.parent).is_terminal = false;
        } else if (field.id == temporary_data->field_ids.Alias ||
                   field.id == temporary_data->field_ids.Name) {
            if (field.id == temporary_data->field_ids.Name)
                container.name = to_string(field.value);
            auto const inserted =
                    temporary_data->containers_by_name.emplace(field.value, event.container_id);
            if (!inserted.second)
                return ParsingError { eta::format("A container with name of alias \"",
                                                  field.value,
                                                  "\" already exist") };
        }
        event.fields.emplace_back(Field { field.id, std::move(field.value) });
    }
    return std::nullopt;
}

static auto parseStandardEvent(std::vector<Field> fields,
                               Trace & trace,
                               eta::Event & event,
                               TemporaryData * temporary_data) -> std::optional<ParsingError> {
    for (auto & field : fields) {
        if (field.id == trace.field_ids.Container) {
            event.container_id = getContainerId(field.value, temporary_data);
            if (!event.container_id.isValid())
                return ParsingError { eta::format("Invalid container identifier ",
                                                  to_string(field.value)) };
            event.fields.emplace_back(Field { field.id, event.container_id });
        } else {
            event.fields.emplace_back(std::move(field));
        }
    }
    return std::nullopt;
}

static auto parseEventLine(std::string_view line,
                           Trace & trace,
                           AdditionalData * additional_data,
                           TemporaryData * temporary_data) -> std::optional<ParsingError> {
    auto index = static_cast<std::size_t>(0);
    consumeSpaces(line, index);
    auto const line_begining = index;
    auto const event_type_id = consumeInt(line, index);
    FORWARD_ERROR(event_type_id);
    auto const & [event_type, inserted] =
            additional_data->event_types.emplace(std::get<Int>(event_type_id), EventType {});
    if (inserted)
        return ParsingError {
            eta::format("Event id ", event_type->first.value(), " was not declared"),
            line,
            line_begining,
            index - line_begining
        };
    auto event = Event { Date(0),
                         EventId::invalid(),
                         {},
                         EventId::invalid(),
                         eta::EventType::Other,
                         ContainerId::invalid(),
                         {} };

    event.fields.emplace_back(Field { temporary_data->field_ids.PajeClass,
                                      to_string(event_type->second.event_class) });

    std::vector<Field> fields;

    for (auto const field : event_type->second.fields) {
        consumeSpaces(line, index);
        auto value_or_error = consumeValue(line, index, field.type);
        FORWARD_ERROR(value_or_error);
        if (field.id == temporary_data->field_ids.Time) {
            Assert(field.type == ValueType::Date, "Time field is expected to be a date");
            event.timestamp = std::get<Date>(std::get<Value>(std::move(value_or_error)));
        } else {
            fields.push_back(Field { field.id, std::get<Value>(std::move(value_or_error)) });
        }
    }
    CHECK_LINE_ENDED(line, index);

    event.id = EventId(trace.events.size());
    ;

    switch (event_type->second.event_class) {
        case EventClass::CreateContainer: {
            auto const err = consumeCreateContainerEvent(std::move(fields),
                                                         trace,
                                                         event,
                                                         event_type->second,
                                                         temporary_data);
            FORWARD_SIMPLE_ERROR(err);
            Assert(event.container_id.isValid(), "This event must have a container");
        } break;
        case EventClass::StartLink: [[fallthrough]];
        case EventClass::EndLink: {
            auto const err = consumeLinkEvent(std::move(fields),
                                              trace,
                                              event,
                                              event_type->second,
                                              temporary_data);
            FORWARD_SIMPLE_ERROR(err);
            Assert(isIn(event.type, eta::EventType::SendMessage, eta::EventType::ReceiveMessage),
                   "");
            Assert(event.container_id.isValid(), "This event must have a container");
        } break;
        case EventClass::ResetState: [[fallthrough]];
        case EventClass::SetState: [[fallthrough]];
        case EventClass::PopState: [[fallthrough]];
        case EventClass::PushState: {
            auto const err = consumeStateEvent(std::move(fields),
                                               trace,
                                               event,
                                               event_type->second,
                                               temporary_data);
            FORWARD_SIMPLE_ERROR(err);
            Assert(event.container_id.isValid(), "This event must have a container");
        } break;
        case EventClass::DestroyContainer: [[fallthrough]];
        case EventClass::DefineVariableType: [[fallthrough]];
        case EventClass::DefineContainerType: [[fallthrough]];
        case EventClass::DefineEntityValue: [[fallthrough]];
        case EventClass::DefineEventType: [[fallthrough]];
        case EventClass::DefineLinkType: [[fallthrough]];
        case EventClass::DefineStateType:
            event.type = eta::EventType::Other;
            parseStandardEvent(std::move(fields), trace, event, temporary_data);
            break;
        case EventClass::AddVariable: [[fallthrough]];
        case EventClass::NewEvent: [[fallthrough]];
        case EventClass::SetVariable: [[fallthrough]];
        case EventClass::SubVariable:
            event.type = eta::EventType::Other;
            parseStandardEvent(std::move(fields), trace, event, temporary_data);
            Assert(event.container_id.isValid(), "This event must have a container");
            break;
        default: Assert(false, "Unknown PajeEventClass");
    };

    trace.push_back_event(event, event.container_id);

    return std::nullopt;
}

auto loadTrace(Path const & path) -> std::variant<Trace, ParsingError> {
    auto file = std::ifstream(path.c_str());
    if (!file.is_open()) {
        auto err = ParsingError { eta::format("Failed to open file ",
                                              path.str(),
                                              " (current directory is ",
                                              currentDirectory().str(),
                                              ')') };
        err.file = path;
        return err;
    }

    auto line = std::string {};
    auto trace = Trace {};
    auto state = ParsingState::InHeader;
    auto line_number = static_cast<std::size_t>(0);

    auto additional_data = new AdditionalData {};
    auto temporary_data = TemporaryData {};

    registerPajeFieldIds(temporary_data.field_ids, trace);

    while (std::getline(file, line)) {
        ++line_number;

        auto const is_white_space = [&]() {
            for (auto const c : line)
                if (!isWhiteSpace(c))
                    return false;
            return true;
        }();

        if (!is_white_space) {
            auto index = static_cast<std::size_t>(0u);
            consumeSpaces(line, index);

            if (line[index] != '%') {
                state = ParsingState::InBody;
                break;
            }

            switch (state) {
                case ParsingState::InHeader: {
                    auto const res =
                            parseHeaderFirstLine(line, index, additional_data, &temporary_data);
                    FORWARD_SIMPLE_ERROR(res);
                    state = ParsingState::InEventDef;
                } break;
                case ParsingState::InEventDef: {
                    auto const res = parseEventDefLine(line, index, trace, &temporary_data);
                    FORWARD_ERROR(res);
                    state = std::get<ParsingState>(res);
                } break;
                default: Assert(false, "");
            }
        }
    }

    temporary_data.containers_by_name.emplace(String("0"), ContainerId::invalid());
    temporary_data.containers_by_name.emplace(Int(0), ContainerId::invalid());

    do {
        ++line_number;

        auto const is_white_space = [&]() {
            for (auto const c : line)
                if (!isWhiteSpace(c))
                    return false;
            return true;
        }();

        if (!is_white_space)
            parseEventLine(line, trace, additional_data, &temporary_data);
    } while (std::getline(file, line));

    if (state != ParsingState::InBody) {
        auto err = ParsingError { "Unexpected end of file", line, 0, line.size() };
        err.line = line_number;
        err.file = path;
    }

    for (auto const & c : temporary_data.collective_comm_by_key) {
        trace.communications.emplace_back(c.second);
    }

    Assert(temporary_data.msg_sent_by_key.empty(), "");
    Assert(temporary_data.msg_recv_by_key.empty(), "");
    Assert(temporary_data.msg_end_keys.empty(), "");

    trace.additional_data = std::unique_ptr<TraceAdditionalData>(additional_data);
    trace.name = path;
    trace.path = path;
    trace.type = "Paje";
    log_debug("Built ",
              trace.type,
              " trace named '",
              trace.name,
              "' from ",
              trace.path,
              " with ",
              trace.events.size(),
              " events");

    return std::variant<Trace, ParsingError>(std::move(trace));
}

auto accept(Path const & path) -> bool { return path.extension() == "trace"; }

}  // namespace eta::paje
