#include "parsing_error.hpp"

namespace eta {

auto operator<<(std::ostream & os, ParsingError const & err) -> std::ostream & {
    if (err.file)
        os << err.file;
    if (err.line != 0)
        os << ':' << err.line << ':' << err.column;
    os << ": " << err.what << '\n';
    os << err.src << '\n';
    auto i = 0u;
    while (i++ < err.column)
        os << ' ';
    do {
        os << '^';
    } while (++i < err.column + err.length);
    os << std::endl;
    return os;
}

}  // namespace eta
