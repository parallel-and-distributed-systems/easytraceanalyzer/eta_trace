#include "otf2.hpp"

#include <otf2/otf2.h>

#include <algorithm>
#include <cassert>
#include <map>

#include "parsing.hpp"
#include "parsing_error.hpp"

namespace eta::otf2 {

auto accept(Path const & path) -> bool { return path.extension() == "otf2"; }

#define OTF2_ENTER_CALLBACK() \
    do {                      \
        log_debug(__func__);  \
    } while (0)

#define OTF2_CHECK(call)                                     \
    do {                                                     \
        OTF2_ErrorCode err = call;                           \
        CriticalAssert(err == OTF2_SUCCESS,                  \
                       "Error while parsing OTF2 trace in ", \
                       __FILE__,                             \
                       ":",                                  \
                       __LINE__);                            \
    } while (0)

#define NOT_IMPLEMENTED()						\
  do {									\
    static int first_time = 1;						\
    if(first_time)  log_warning(__func__, " (", ((OTF2_Trace *)userData)->_filename, ") not implemented !"); \
    first_time = 0;							\
    return OTF2_CALLBACK_SUCCESS;					\
  } while (0)

struct OTF2_Location;
struct OTF2_LocationGroup;
/*!
 * \struct OTF2_SystemTreeNode
 * \brief Contains the definition of a machine (equivalent in Paje : Container)
 */
struct OTF2_SystemTreeNode {
    /*! \brief Id of the node */
    OTF2_SystemTreeNodeRef _node_id;
    /*! \brief Name of the node */
    OTF2_StringRef _name_id;
    /*! \brief id of the parent node */
    OTF2_SystemTreeNodeRef _parent;
    /*! \brief child system tree nodes */
    std::map<OTF2_SystemTreeNodeRef, OTF2_SystemTreeNode *> _child_nodes;
    /*! \brief child location groups */
    std::map<OTF2_LocationGroupRef, OTF2_LocationGroup *> _location_group;
};

/*!
 * \struct OTF2_LocationGroup
 * \brief Contains the definition of a processGroup (equivalent in Paje : Container)
 */
struct OTF2_LocationGroup {
    /*! \brief Id of the LocationGroup */
    OTF2_LocationGroupRef _group_id;
    /*! \brief Name of the LocationGroup */
    uint32_t _name_id;
    /*! \brief Child locations */
    std::map<OTF2_LocationRef, OTF2_Location *> _location;
    /*! \brief Id of the parent node */
    OTF2_SystemTreeNodeRef _node_id;
};

/*!
 * \struct OTF2_Location
 * \brief Contains the definition of a process (equivalent in Paje : Container)
 */
struct OTF2_Location {
    /*! \brief Id of the Location */
    OTF2_LocationRef _location_id;
    /*! \brief Name of the Location */
    uint32_t _name_id;
    /*! \brief Id of the parent group */
    OTF2_LocationGroupRef _group_id;
    /*! \brief Number of events in this Location */
    uint64_t _number_of_events;
    /*! \brief Container corresponding to this Location */
    ContainerId _container;
};

/*!
 * \struct OTF2_Function
 * \brief Contains the definition of a function (equivalent in Paje : State)
 */
struct OTF2_Function {
    OTF2_RegionRef _region_id;
    /*! \brief Name of the state */
    OTF2_StringRef _name_id;
    /*! \brief Alternative name of the region (e.g. mangled name */
    OTF2_StringRef _canonicalName;
    /*! \brief A more detailed description of this region */
    OTF2_StringRef _region_description;
    /*! \brief Region role. */
    OTF2_RegionRole _regionRole;
    /*! \brief Paradigm. */
    OTF2_Paradigm _paradigm;
    /*! \brief Region flags. */
    OTF2_RegionFlag _regionFlags;
    /*! \brief The source file where this region was declared */
    OTF2_StringRef _sourceFile;
    /*! \brieg Starting line number of this region in the source file. */
    uint32_t _begin_line_number;
    /*! \brieg Ending line number of this region in the source file. */
    uint32_t _end_line_number;
};

struct OTF2_MetricMember {
    OTF2_MetricMemberRef _id;
    OTF2_StringRef _name;
    OTF2_StringRef _description;
    OTF2_MetricType _metricType;
    OTF2_MetricMode _metricMode;
    OTF2_Type _valueType;
    OTF2_Base _base;
    int64_t _exponent;
    OTF2_StringRef _unit;
};

struct OTF2_MetricClass {
    OTF2_MetricRef _id;
    std::vector<OTF2_MetricMemberRef> _metricMembers;
    OTF2_MetricOccurrence _metricOccurrence;
    OTF2_RecorderKind _recorderKind;
};

struct OTF2_Group {
    OTF2_GroupRef _id;
    OTF2_StringRef _name;
    OTF2_GroupType _type;
    OTF2_Paradigm _paradigm;
    OTF2_GroupFlag _flags;
    std::vector<uint64_t> _members;
};

struct OTF2_Comm {
    OTF2_CommRef _id;
    OTF2_StringRef _name;
    OTF2_GroupRef _group;
    OTF2_CommRef _parent;
};

struct message {
    ContainerId src;
    ContainerId dst;
    uint32_t tag;
    OTF2_CommRef comm;
    eta::EventId event;
};

struct OTF2_Trace {
    std::unique_ptr<Trace> _trace;
    std::string _filename;
    /*!
     * Reader for the OTF2 trace
     */
    OTF2_GlobalDefReader * _global_def_reader;
    OTF2_GlobalDefReaderCallbacks * _global_def_callbacks;

    OTF2_GlobalEvtReader * _global_evt_reader;
    OTF2_GlobalEvtReaderCallbacks * _global_evt_callbacks;

    OTF2_Reader * _reader;
    /*!
     * Maps in order to easily retrieve the events.
     */
    std::map<OTF2_SystemTreeNodeRef, OTF2_SystemTreeNode *> _system_tree_node;
    std::map<OTF2_LocationGroupRef, OTF2_LocationGroup *> _location_group;
    std::map<OTF2_LocationRef, OTF2_Location *> _location;

    std::map<OTF2_MetricMemberRef, OTF2_MetricMember> _metric_member;
    std::map<OTF2_MetricRef, OTF2_MetricClass> _metric_class;

    std::map<OTF2_RegionRef, OTF2_Function *> _functions;
    std::map<OTF2_StringRef, std::string> _strings;

    std::map<OTF2_GroupRef, OTF2_Group *> _groups;
    std::map<OTF2_CommRef, OTF2_Comm *> _comms;

    uint64_t _ticks_per_second;
    OTF2_TimeStamp _first_timestamp;

    FieldId functionName;
    FieldId destRank;
    FieldId srcRank;
    // for MPI, e.g in BCast, dest and src are ambiguous, use instead root and proc
    FieldId rootRank;
    FieldId procRank;
    FieldId communicator;
    FieldId msgTag;
    FieldId msgLength;
    FieldId enterFunctionEvent;
    FieldId exitFunctionEvent;
    FieldId mpiEvent;
    std::map<ContainerId, std::vector<message>> sent_messages;
    std::map<ContainerId, std::vector<EventId>> enter_events;
    std::map<ContainerId, std::vector<EventId>> mpi_events;
    std::map<ContainerId, std::vector<EventId>> posixio_events;
    FieldId matchingId;
    FieldId iOMode;

    std::vector<Communication> current_coll_comm;
};

static OTF2_CallbackCode callback_DefTimerResolution(void *, uint64_t, uint64_t, uint64_t);
static OTF2_CallbackCode callback_DefString(void *, OTF2_StringRef, const char *);

// System tree callback
static OTF2_CallbackCode callback_DefSystemTreeNode(void * userData,
                                                    OTF2_SystemTreeNodeRef tree_node_id,
                                                    OTF2_StringRef name_id,
                                                    OTF2_StringRef class_id,
                                                    OTF2_SystemTreeNodeRef parent_node_id);

static OTF2_CallbackCode callback_DefLocationGroup(void * userdata,
                                                   OTF2_LocationGroupRef location_group_identifier,
                                                   OTF2_StringRef name,
                                                   OTF2_LocationGroupType type,
                                                   OTF2_SystemTreeNodeRef system_tree_parent);
static OTF2_CallbackCode callback_DefLocation(void * userData,
                                              OTF2_LocationRef locationIdentifier,
                                              OTF2_StringRef name_id,
                                              OTF2_LocationType location_type,
                                              uint64_t numberOfEvents,
                                              OTF2_LocationGroupRef locationGroup);

// Region callback
static OTF2_CallbackCode callback_DefRegion(void * userData,
                                            OTF2_RegionRef self,
                                            OTF2_StringRef name,
                                            OTF2_StringRef canonicalName,
                                            OTF2_StringRef description,
                                            OTF2_RegionRole regionRole,
                                            OTF2_Paradigm paradigm,
                                            OTF2_RegionFlag regionFlags,
                                            OTF2_StringRef sourceFile,
                                            uint32_t beginLineNumber,
                                            uint32_t endLineNumber);

static OTF2_CallbackCode callback_DefGroup(void * userData,
                                           OTF2_GroupRef group_id,
                                           OTF2_StringRef name_id,
                                           OTF2_GroupType type,
                                           OTF2_Paradigm paradigm,
                                           OTF2_GroupFlag flags,
                                           uint32_t numberOfMembers,
                                           const uint64_t * members);

static OTF2_CallbackCode callback_Comm(void * userData,
                                       OTF2_CommRef self,
                                       OTF2_StringRef name,
                                       OTF2_GroupRef group,
                                       OTF2_CommRef parent);

static OTF2_CallbackCode callback_DefMetricMember(void * userData,
                                                  OTF2_MetricMemberRef self,
                                                  OTF2_StringRef name,
                                                  OTF2_StringRef description,
                                                  OTF2_MetricType metricType,
                                                  OTF2_MetricMode metricMode,
                                                  OTF2_Type valueType,
                                                  OTF2_Base base,
                                                  int64_t exponent,
                                                  OTF2_StringRef unit);

static OTF2_CallbackCode callback_DefMetricClass(void * userData,
                                                 OTF2_MetricRef self,
                                                 uint8_t numberOfMetrics,
                                                 const OTF2_MetricMemberRef * metricMembers,
                                                 OTF2_MetricOccurrence metricOccurrence,
                                                 OTF2_RecorderKind recorderKind);

static OTF2_CallbackCode callback_DefMetricInstance(void * userData,
                                                    OTF2_MetricRef self,
                                                    OTF2_MetricRef metricClass,
                                                    OTF2_LocationRef recorder,
                                                    OTF2_MetricScope metricScope,
                                                    uint64_t scope);

static OTF2_CallbackCode callback_DefMetricClassRecorder(void * userData,
                                                         OTF2_MetricRef metric,
                                                         OTF2_LocationRef recorder);

static auto getTimestamp(OTF2_Trace * t, OTF2_TimeStamp time) -> Date {
    if (t->_first_timestamp == 0) {
        t->_first_timestamp = time;
    }
    return ((double)time - (double)t->_first_timestamp) / t->_ticks_per_second;
}

static auto getSystemTreeNode(OTF2_Trace * t, OTF2_SystemTreeNodeRef ref) -> OTF2_SystemTreeNode * {
    return t->_system_tree_node[ref];
}
static auto setSystemTreeNode(OTF2_Trace * t, OTF2_SystemTreeNodeRef ref, OTF2_SystemTreeNode * s)
        -> void {
    t->_system_tree_node[ref] = s;
}

static auto getLocationGroup(OTF2_Trace * t, OTF2_LocationGroupRef ref) -> OTF2_LocationGroup * {
    return t->_location_group[ref];
}
static auto setLocationGroup(OTF2_Trace * t, OTF2_LocationGroupRef ref, OTF2_LocationGroup * l)
        -> void {
    t->_location_group[ref] = l;
}

static auto getLocation(OTF2_Trace * t, OTF2_LocationRef ref) -> OTF2_Location * {
    return t->_location[ref];
}
static auto setLocation(OTF2_Trace * t, OTF2_LocationRef ref, OTF2_Location * l) -> void {
    t->_location[ref] = l;
}

[[maybe_unused]] static auto getMetricMember(OTF2_Trace * t, OTF2_MetricMemberRef ref)
        -> OTF2_MetricMember {
    return t->_metric_member[ref];
}
[[maybe_unused]] static auto setMetricMember(OTF2_Trace * t,
                                             OTF2_MetricMemberRef ref,
                                             OTF2_MetricMember m) -> void {
    t->_metric_member[ref] = m;
}

[[maybe_unused]] static auto getMetricClass(OTF2_Trace * t, OTF2_MetricRef ref)
        -> OTF2_MetricClass {
    return t->_metric_class[ref];
}
[[maybe_unused]] static auto setMetricClass(OTF2_Trace * t, OTF2_MetricRef ref, OTF2_MetricClass m)
        -> void {
    t->_metric_class[ref] = m;
}

static auto getFunction(OTF2_Trace * t, OTF2_RegionRef ref) -> OTF2_Function * {
    return t->_functions[ref];
}
static auto setFunction(OTF2_Trace * t, OTF2_RegionRef ref, OTF2_Function * f) -> void {
    t->_functions[ref] = f;
}

static auto getString(OTF2_Trace * t, OTF2_StringRef ref) -> std::string {
    return t->_strings[ref];
}
static auto setString(OTF2_Trace * t, OTF2_StringRef ref, std::string str) -> void {
    t->_strings[ref] = str;
}

static auto getGroup(OTF2_Trace * t, OTF2_GroupRef ref) -> OTF2_Group * { return t->_groups[ref]; }
static auto setGroup(OTF2_Trace * t, OTF2_GroupRef ref, OTF2_Group * group) -> void {
    t->_groups[ref] = group;
}

static auto getComm(OTF2_Trace * t, OTF2_CommRef ref) -> OTF2_Comm * { return t->_comms[ref]; }
static auto setComm(OTF2_Trace * t, OTF2_CommRef ref, OTF2_Comm * comm) -> void {
    t->_comms[ref] = comm;
}

static auto getContainerFromRank(OTF2_Trace * t, OTF2_CommRef comm_ref, uint32_t rank)
        -> OTF2_Location * {
    auto comm = getComm(t, comm_ref);
    auto grp = getGroup(t, comm->_group);

    if (rank > grp->_members.size()) {
        return t->_location[rank];
    } else {
        return t->_location[grp->_members[rank]];
    }
}

static OTF2_CallbackCode callback_unknown(OTF2_LocationRef locationID,
                                          OTF2_TimeStamp time,
                                          void * userData,
                                          OTF2_AttributeList * attributeList);

static OTF2_CallbackCode callback_bufferFlush(OTF2_LocationRef locationID,
                                              OTF2_TimeStamp time,
                                              void * userData,
                                              OTF2_AttributeList * attributeList,
                                              OTF2_TimeStamp stopTime);

static OTF2_CallbackCode callback_MeasurementOnOff(OTF2_LocationRef locationID,
                                                   OTF2_TimeStamp time,
                                                   void * userData,
                                                   OTF2_AttributeList * attributeList,
                                                   OTF2_MeasurementMode measurementMode);

static OTF2_CallbackCode callback_Enter(OTF2_LocationRef locationID,
                                        OTF2_TimeStamp time,
                                        void * userData,
                                        OTF2_AttributeList * attributeList,
                                        OTF2_RegionRef region);

static OTF2_CallbackCode callback_Leave(OTF2_LocationRef locationID,
                                        OTF2_TimeStamp time,
                                        void * userData,
                                        OTF2_AttributeList * attributeList,
                                        OTF2_RegionRef region);

static OTF2_CallbackCode callback_MpiSend(OTF2_LocationRef locationID,
                                          OTF2_TimeStamp time,
                                          void * userData,
                                          OTF2_AttributeList * attributeList,
                                          uint32_t receiver,
                                          OTF2_CommRef communicator,
                                          uint32_t msgTag,
                                          uint64_t msgLength);

static OTF2_CallbackCode callback_MpiIsend(OTF2_LocationRef locationID,
                                           OTF2_TimeStamp time,
                                           void * userData,
                                           OTF2_AttributeList * attributeList,
                                           uint32_t receiver,
                                           OTF2_CommRef communicator,
                                           uint32_t msgTag,
                                           uint64_t msgLength,
                                           uint64_t requestID);

static OTF2_CallbackCode callback_MpiIsendComplete(OTF2_LocationRef locationID,
                                                   OTF2_TimeStamp time,
                                                   void * userData,
                                                   OTF2_AttributeList * attributeList,
                                                   uint64_t requestID);

static OTF2_CallbackCode callback_MpiIrecvRequest(OTF2_LocationRef locationID,
                                                  OTF2_TimeStamp time,
                                                  void * userData,
                                                  OTF2_AttributeList * attributeList,
                                                  uint64_t requestID);
static OTF2_CallbackCode callback_MpiRecv(OTF2_LocationRef locationID,
                                          OTF2_TimeStamp time,
                                          void * userData,
                                          OTF2_AttributeList * attributeList,
                                          uint32_t sender,
                                          OTF2_CommRef communicator,
                                          uint32_t msgTag,
                                          uint64_t msgLength);

static OTF2_CallbackCode callback_MpiIrecv(OTF2_LocationRef locationID,
                                           OTF2_TimeStamp time,
                                           void * userData,
                                           OTF2_AttributeList * attributeList,
                                           uint32_t sender,
                                           OTF2_CommRef communicator,
                                           uint32_t msgTag,
                                           uint64_t msgLength,
                                           uint64_t requestID);

static OTF2_CallbackCode callback_MpiRequestTest(OTF2_LocationRef locationID,
                                                 OTF2_TimeStamp time,
                                                 void * userData,
                                                 OTF2_AttributeList * attributeList,
                                                 uint64_t requestID);

static OTF2_CallbackCode callback_MpiRequestCancelled(OTF2_LocationRef locationID,
                                                      OTF2_TimeStamp time,
                                                      void * userData,
                                                      OTF2_AttributeList * attributeList,
                                                      uint64_t requestID);

static OTF2_CallbackCode callback_MpiCollectiveBegin(OTF2_LocationRef locationID,
                                                     OTF2_TimeStamp time,
                                                     void * userData,
                                                     OTF2_AttributeList * attributeList);

static OTF2_CallbackCode callback_MpiCollectiveEnd(OTF2_LocationRef locationID,
                                                   OTF2_TimeStamp time,
                                                   void * userData,
                                                   OTF2_AttributeList * attributeList,
                                                   OTF2_CollectiveOp collectiveOp,
                                                   OTF2_CommRef communicator,
                                                   uint32_t root,
                                                   uint64_t sizeSent,
                                                   uint64_t sizeReceived);

static OTF2_CallbackCode callback_OmpFork(OTF2_LocationRef locationID,
                                          OTF2_TimeStamp time,
                                          void * userData,
                                          OTF2_AttributeList * attributeList,
                                          uint32_t numberOfRequestedThreads);

static OTF2_CallbackCode callback_OmpJoin(OTF2_LocationRef locationID,
                                          OTF2_TimeStamp time,
                                          void * userData,
                                          OTF2_AttributeList * attributeList);

static OTF2_CallbackCode callback_OmpAcquireLock(OTF2_LocationRef locationID,
                                                 OTF2_TimeStamp time,
                                                 void * userData,
                                                 OTF2_AttributeList * attributeList,
                                                 uint32_t lockID,
                                                 uint32_t acquisitionOrder);

static OTF2_CallbackCode callback_OmpReleaseLock(OTF2_LocationRef locationID,
                                                 OTF2_TimeStamp time,
                                                 void * userData,
                                                 OTF2_AttributeList * attributeList,
                                                 uint32_t lockID,
                                                 uint32_t acquisitionOrder);

static OTF2_CallbackCode callback_OmpTaskCreate(OTF2_LocationRef locationID,
                                                OTF2_TimeStamp time,
                                                void * userData,
                                                OTF2_AttributeList * attributeList,
                                                uint64_t taskID);

static OTF2_CallbackCode callback_OmpTaskSwitch(OTF2_LocationRef locationID,
                                                OTF2_TimeStamp time,
                                                void * userData,
                                                OTF2_AttributeList * attributeList,
                                                uint64_t taskID);

static OTF2_CallbackCode callback_OmpTaskComplete(OTF2_LocationRef locationID,
                                                  OTF2_TimeStamp time,
                                                  void * userData,
                                                  OTF2_AttributeList * attributeList,
                                                  uint64_t taskID);

static OTF2_CallbackCode callback_Metric(OTF2_LocationRef locationID,
                                         OTF2_TimeStamp time,
                                         void * userData,
                                         OTF2_AttributeList * attributeList,
                                         OTF2_MetricRef metric,
                                         uint8_t numberOfMetrics,
                                         const OTF2_Type * typeIDs,
                                         const OTF2_MetricValue * metricValues);

static OTF2_CallbackCode callback_ParameterString(OTF2_LocationRef locationID,
                                                  OTF2_TimeStamp time,
                                                  void * userData,
                                                  OTF2_AttributeList * attributeList,
                                                  OTF2_ParameterRef parameter,
                                                  OTF2_StringRef string);

static OTF2_CallbackCode callback_ParameterInt(OTF2_LocationRef locationID,
                                               OTF2_TimeStamp time,
                                               void * userData,
                                               OTF2_AttributeList * attributeList,
                                               OTF2_ParameterRef parameter,
                                               int64_t value);

static OTF2_CallbackCode callback_ParameterUnsignedInt(OTF2_LocationRef locationID,
                                                       OTF2_TimeStamp time,
                                                       void * userData,
                                                       OTF2_AttributeList * attributeList,
                                                       OTF2_ParameterRef parameter,
                                                       uint64_t value);

static OTF2_CallbackCode callback_RmaWinCreate(OTF2_LocationRef locationID,
                                               OTF2_TimeStamp time,
                                               void * userData,
                                               OTF2_AttributeList * attributeList,
                                               OTF2_RmaWinRef win);

static OTF2_CallbackCode callback_RmaWinDestroy(OTF2_LocationRef locationID,
                                                OTF2_TimeStamp time,
                                                void * userData,
                                                OTF2_AttributeList * attributeList,
                                                OTF2_RmaWinRef win);

static OTF2_CallbackCode callback_RmaCollectiveBegin(OTF2_LocationRef locationID,
                                                     OTF2_TimeStamp time,
                                                     void * userData,
                                                     OTF2_AttributeList * attributeList);

static OTF2_CallbackCode callback_RmaCollectiveEnd(OTF2_LocationRef locationID,
                                                   OTF2_TimeStamp time,
                                                   void * userData,
                                                   OTF2_AttributeList * attributeList,
                                                   OTF2_CollectiveOp collectiveOp,
                                                   OTF2_RmaSyncLevel syncLevel,
                                                   OTF2_RmaWinRef win,
                                                   uint32_t root,
                                                   uint64_t bytesSent,
                                                   uint64_t bytesReceived);

static OTF2_CallbackCode callback_RmaGroupSync(OTF2_LocationRef locationID,
                                               OTF2_TimeStamp time,
                                               void * userData,
                                               OTF2_AttributeList * attributeList,
                                               OTF2_RmaSyncLevel syncLevel,
                                               OTF2_RmaWinRef win,
                                               OTF2_GroupRef group);

static OTF2_CallbackCode callback_RmaRequestLock(OTF2_LocationRef locationID,
                                                 OTF2_TimeStamp time,
                                                 void * userData,
                                                 OTF2_AttributeList * attributeList,
                                                 OTF2_RmaWinRef win,
                                                 uint32_t remote,
                                                 uint64_t lockId,
                                                 OTF2_LockType lockType);

static OTF2_CallbackCode callback_RmaAcquireLock(OTF2_LocationRef locationID,
                                                 OTF2_TimeStamp time,
                                                 void * userData,
                                                 OTF2_AttributeList * attributeList,
                                                 OTF2_RmaWinRef win,
                                                 uint32_t remote,
                                                 uint64_t lockId,
                                                 OTF2_LockType lockType);

static OTF2_CallbackCode callback_RmaTryLock(OTF2_LocationRef locationID,
                                             OTF2_TimeStamp time,
                                             void * userData,
                                             OTF2_AttributeList * attributeList,
                                             OTF2_RmaWinRef win,
                                             uint32_t remote,
                                             uint64_t lockId,
                                             OTF2_LockType lockType);

static OTF2_CallbackCode callback_RmaReleaseLock(OTF2_LocationRef locationID,
                                                 OTF2_TimeStamp time,
                                                 void * userData,
                                                 OTF2_AttributeList * attributeList,
                                                 OTF2_RmaWinRef win,
                                                 uint32_t remote,
                                                 uint64_t lockId);

static OTF2_CallbackCode callback_RmaSync(OTF2_LocationRef locationID,
                                          OTF2_TimeStamp time,
                                          void * userData,
                                          OTF2_AttributeList * attributeList,
                                          OTF2_RmaWinRef win,
                                          uint32_t remote,
                                          OTF2_RmaSyncType syncType);

static OTF2_CallbackCode callback_RmaWaitChange(OTF2_LocationRef locationID,
                                                OTF2_TimeStamp time,
                                                void * userData,
                                                OTF2_AttributeList * attributeList,
                                                OTF2_RmaWinRef win);

static OTF2_CallbackCode callback_RmaPut(OTF2_LocationRef locationID,
                                         OTF2_TimeStamp time,
                                         void * userData,
                                         OTF2_AttributeList * attributeList,
                                         OTF2_RmaWinRef win,
                                         uint32_t remote,
                                         uint64_t bytes,
                                         uint64_t matchingId);

static OTF2_CallbackCode callback_RmaGet(OTF2_LocationRef locationID,
                                         OTF2_TimeStamp time,
                                         void * userData,
                                         OTF2_AttributeList * attributeList,
                                         OTF2_RmaWinRef win,
                                         uint32_t remote,
                                         uint64_t bytes,
                                         uint64_t matchingId);

static OTF2_CallbackCode callback_RmaAtomic(OTF2_LocationRef locationID,
                                            OTF2_TimeStamp time,
                                            void * userData,
                                            OTF2_AttributeList * attributeList,
                                            OTF2_RmaWinRef win,
                                            uint32_t remote,
                                            OTF2_RmaAtomicType type,
                                            uint64_t bytesSent,
                                            uint64_t bytesReceived,
                                            uint64_t matchingId);

static OTF2_CallbackCode callback_RmaOpCompleteBlocking(OTF2_LocationRef locationID,
                                                        OTF2_TimeStamp time,
                                                        void * userData,
                                                        OTF2_AttributeList * attributeList,
                                                        OTF2_RmaWinRef win,
                                                        uint64_t matchingId);

static OTF2_CallbackCode callback_RmaOpCompleteNonBlocking(OTF2_LocationRef locationID,
                                                           OTF2_TimeStamp time,
                                                           void * userData,
                                                           OTF2_AttributeList * attributeList,
                                                           OTF2_RmaWinRef win,
                                                           uint64_t matchingId);

static OTF2_CallbackCode callback_RmaOpTest(OTF2_LocationRef locationID,
                                            OTF2_TimeStamp time,
                                            void * userData,
                                            OTF2_AttributeList * attributeList,
                                            OTF2_RmaWinRef win,
                                            uint64_t matchingId);

static OTF2_CallbackCode callback_RmaOpCompleteRemote(OTF2_LocationRef locationID,
                                                      OTF2_TimeStamp time,
                                                      void * userData,
                                                      OTF2_AttributeList * attributeList,
                                                      OTF2_RmaWinRef win,
                                                      uint64_t matchingId);

static OTF2_CallbackCode callback_ThreadFork(OTF2_LocationRef locationID,
                                             OTF2_TimeStamp time,
                                             void * userData,
                                             OTF2_AttributeList * attributeList,
                                             OTF2_Paradigm model,
                                             uint32_t numberOfRequestedThreads);

static OTF2_CallbackCode callback_ThreadJoin(OTF2_LocationRef locationID,
                                             OTF2_TimeStamp time,
                                             void * userData,
                                             OTF2_AttributeList * attributeList,
                                             OTF2_Paradigm model);

static OTF2_CallbackCode callback_ThreadTeamBegin(OTF2_LocationRef locationID,
                                                  OTF2_TimeStamp time,
                                                  void * userData,
                                                  OTF2_AttributeList * attributeList,
                                                  OTF2_CommRef threadTeam);

static OTF2_CallbackCode callback_ThreadTeamEnd(OTF2_LocationRef locationID,
                                                OTF2_TimeStamp time,
                                                void * userData,
                                                OTF2_AttributeList * attributeList,
                                                OTF2_CommRef threadTeam);

static OTF2_CallbackCode callback_ThreadAcquireLock(OTF2_LocationRef locationID,
                                                    OTF2_TimeStamp time,
                                                    void * userData,
                                                    OTF2_AttributeList * attributeList,
                                                    OTF2_Paradigm model,
                                                    uint32_t lockID,
                                                    uint32_t acquisitionOrder);

static OTF2_CallbackCode callback_ThreadReleaseLock(OTF2_LocationRef locationID,
                                                    OTF2_TimeStamp time,
                                                    void * userData,
                                                    OTF2_AttributeList * attributeList,
                                                    OTF2_Paradigm model,
                                                    uint32_t lockID,
                                                    uint32_t acquisitionOrder);

static OTF2_CallbackCode callback_ThreadTaskCreate(OTF2_LocationRef locationID,
                                                   OTF2_TimeStamp time,
                                                   void * userData,
                                                   OTF2_AttributeList * attributeList,
                                                   OTF2_CommRef threadTeam,
                                                   uint32_t creatingThread,
                                                   uint32_t generationNumber);

static OTF2_CallbackCode callback_ThreadTaskSwitch(OTF2_LocationRef locationID,
                                                   OTF2_TimeStamp time,
                                                   void * userData,
                                                   OTF2_AttributeList * attributeList,
                                                   OTF2_CommRef threadTeam,
                                                   uint32_t creatingThread,
                                                   uint32_t generationNumber);

static OTF2_CallbackCode callback_ThreadTaskComplete(OTF2_LocationRef locationID,
                                                     OTF2_TimeStamp time,
                                                     void * userData,
                                                     OTF2_AttributeList * attributeList,
                                                     OTF2_CommRef threadTeam,
                                                     uint32_t creatingThread,
                                                     uint32_t generationNumber);

static OTF2_CallbackCode callback_ThreadCreate(OTF2_LocationRef locationID,
                                               OTF2_TimeStamp time,
                                               void * userData,
                                               OTF2_AttributeList * attributeList,
                                               OTF2_CommRef threadContingent,
                                               uint64_t sequenceCount);

static OTF2_CallbackCode callback_ThreadBegin(OTF2_LocationRef locationID,
                                              OTF2_TimeStamp time,
                                              void * userData,
                                              OTF2_AttributeList * attributeList,
                                              OTF2_CommRef threadContingent,
                                              uint64_t sequenceCount);

static OTF2_CallbackCode callback_ThreadWait(OTF2_LocationRef locationID,
                                             OTF2_TimeStamp time,
                                             void * userData,
                                             OTF2_AttributeList * attributeList,
                                             OTF2_CommRef threadContingent,
                                             uint64_t sequenceCount);

static OTF2_CallbackCode callback_ThreadEnd(OTF2_LocationRef locationID,
                                            OTF2_TimeStamp time,
                                            void * userData,
                                            OTF2_AttributeList * attributeList,
                                            OTF2_CommRef threadContingent,
                                            uint64_t sequenceCount);

static OTF2_CallbackCode callback_CallingContextEnter(OTF2_LocationRef locationID,
                                                      OTF2_TimeStamp time,
                                                      void * userData,
                                                      OTF2_AttributeList * attributeList,
                                                      OTF2_CallingContextRef callingContext,
                                                      uint32_t unwindDistance);

static OTF2_CallbackCode callback_CallingContextLeave(OTF2_LocationRef locationID,
                                                      OTF2_TimeStamp time,
                                                      void * userData,
                                                      OTF2_AttributeList * attributeList,
                                                      OTF2_CallingContextRef callingContext);

static OTF2_CallbackCode callback_CallingContextSample(
        OTF2_LocationRef locationID,
        OTF2_TimeStamp time,
        void * userData,
        OTF2_AttributeList * attributeList,
        OTF2_CallingContextRef callingContext,
        uint32_t unwindDistance,
        OTF2_InterruptGeneratorRef interruptGenerator);

static OTF2_CallbackCode callback_IoCreateHandle(OTF2_LocationRef locationID,
                                                 OTF2_TimeStamp time,
                                                 void * userData,
                                                 OTF2_AttributeList * attributeList,
                                                 OTF2_IoHandleRef handle,
                                                 OTF2_IoAccessMode mode,
                                                 OTF2_IoCreationFlag creationFlags,
                                                 OTF2_IoStatusFlag statusFlags);

static OTF2_CallbackCode callback_IoDestroyHandle(OTF2_LocationRef locationID,
                                                  OTF2_TimeStamp time,
                                                  void * userData,
                                                  OTF2_AttributeList * attributeList,
                                                  OTF2_IoHandleRef handle);

static OTF2_CallbackCode callback_IoDuplicateHandle(OTF2_LocationRef locationID,
                                                    OTF2_TimeStamp time,
                                                    void * userData,
                                                    OTF2_AttributeList * attributeList,
                                                    OTF2_IoHandleRef oldHandle,
                                                    OTF2_IoHandleRef newHandle,
                                                    OTF2_IoStatusFlag statusFlags);

static OTF2_CallbackCode callback_IoSeek(OTF2_LocationRef locationID,
                                         OTF2_TimeStamp time,
                                         void * userData,
                                         OTF2_AttributeList * attributeList,
                                         OTF2_IoHandleRef handle,
                                         int64_t offsetRequest,
                                         OTF2_IoSeekOption whence,
                                         uint64_t offsetResult);

static OTF2_CallbackCode callback_IoChangeStatusFlags(OTF2_LocationRef locationID,
                                                      OTF2_TimeStamp time,
                                                      void * userData,
                                                      OTF2_AttributeList * attributeList,
                                                      OTF2_IoHandleRef handle,
                                                      OTF2_IoStatusFlag statusFlags);

static OTF2_CallbackCode callback_IoDeleteFile(OTF2_LocationRef locationID,
                                               OTF2_TimeStamp time,
                                               void * userData,
                                               OTF2_AttributeList * attributeList,
                                               OTF2_IoParadigmRef ioParadigm,
                                               OTF2_IoFileRef file);

static OTF2_CallbackCode callback_IoOperationBegin(OTF2_LocationRef locationID,
                                                   OTF2_TimeStamp time,
                                                   void * userData,
                                                   OTF2_AttributeList * attributeList,
                                                   OTF2_IoHandleRef handle,
                                                   OTF2_IoOperationMode mode,
                                                   OTF2_IoOperationFlag operationFlags,
                                                   uint64_t bytesRequest,
                                                   uint64_t matchingId);

static OTF2_CallbackCode callback_IoOperationTest(OTF2_LocationRef locationID,
                                                  OTF2_TimeStamp time,
                                                  void * userData,
                                                  OTF2_AttributeList * attributeList,
                                                  OTF2_IoHandleRef handle,
                                                  uint64_t matchingId);

static OTF2_CallbackCode callback_IoOperationIssued(OTF2_LocationRef locationID,
                                                    OTF2_TimeStamp time,
                                                    void * userData,
                                                    OTF2_AttributeList * attributeList,
                                                    OTF2_IoHandleRef handle,
                                                    uint64_t matchingId);

static OTF2_CallbackCode callback_IoOperationComplete(OTF2_LocationRef locationID,
                                                      OTF2_TimeStamp time,
                                                      void * userData,
                                                      OTF2_AttributeList * attributeList,
                                                      OTF2_IoHandleRef handle,
                                                      uint64_t bytesResult,
                                                      uint64_t matchingId);

static OTF2_CallbackCode callback_IoOperationCancelled(OTF2_LocationRef locationID,
                                                       OTF2_TimeStamp time,
                                                       void * userData,
                                                       OTF2_AttributeList * attributeList,
                                                       OTF2_IoHandleRef handle,
                                                       uint64_t matchingId);

static OTF2_CallbackCode callback_IoAcquireLock(OTF2_LocationRef locationID,
                                                OTF2_TimeStamp time,
                                                void * userData,
                                                OTF2_AttributeList * attributeList,
                                                OTF2_IoHandleRef handle,
                                                OTF2_LockType lockType);

static OTF2_CallbackCode callback_IoReleaseLock(OTF2_LocationRef locationID,
                                                OTF2_TimeStamp time,
                                                void * userData,
                                                OTF2_AttributeList * attributeList,
                                                OTF2_IoHandleRef handle,
                                                OTF2_LockType lockType);

static OTF2_CallbackCode callback_IoTryLock(OTF2_LocationRef locationID,
                                            OTF2_TimeStamp time,
                                            void * userData,
                                            OTF2_AttributeList * attributeList,
                                            OTF2_IoHandleRef handle,
                                            OTF2_LockType lockType);

static OTF2_CallbackCode callback_ProgramBegin(OTF2_LocationRef locationID,
                                               OTF2_TimeStamp time,
                                               void * userData,
                                               OTF2_AttributeList * attributeList,
                                               OTF2_StringRef programName,
                                               uint32_t numberOfArguments,
                                               const OTF2_StringRef * programArguments);

static OTF2_CallbackCode callback_ProgramEnd(OTF2_LocationRef locationID,
                                             OTF2_TimeStamp time,
                                             void * userData,
                                             OTF2_AttributeList * attributeList,
                                             int64_t exitStatus);

auto loadTrace(Path const & path) -> std::variant<Trace, ParsingError> {
    OTF2_Trace t;
    t._reader = OTF2_Reader_Open(path.c_str());
    if (t._reader == nullptr) {
        auto err = ParsingError { eta::format("Failed to open file ",
                                              path.str(),
                                              " (current directory is ",
                                              currentDirectory().str(),
                                              ')') };
        err.file = path;
        return err;
    }
    t._trace = std::make_unique<Trace>();

    t.functionName = t._trace->field_ids.FunctionName;
    t.destRank = t._trace->fields.register_new_field_name("dest");
    t.srcRank = t._trace->fields.register_new_field_name("src");
    t.rootRank = t._trace->fields.register_new_field_name("root");
    t.procRank = t._trace->fields.register_new_field_name("proc");
    t.communicator = t._trace->fields.register_new_field_name("communicator");
    t.msgTag = t._trace->field_ids.CommunicationTag;
    t.msgLength = t._trace->field_ids.CommunicationLength;
    t.matchingId = t._trace->fields.register_new_field_name("id");
    t.iOMode = t._trace->fields.register_new_field_name("mode");
    t.enterFunctionEvent = t._trace->field_ids.EnterFunctionEvent;
    t.exitFunctionEvent = t._trace->field_ids.ExitFunctionEvent;
    t.mpiEvent = t._trace->fields.register_new_field_name("mpiEvent");

    uint64_t number_of_locations;
    OTF2_Reader_GetNumberOfLocations(t._reader, &number_of_locations);

    t._global_def_reader = OTF2_Reader_GetGlobalDefReader(t._reader);
    t._global_def_callbacks = OTF2_GlobalDefReaderCallbacks_New();

    OTF2_CHECK(
            OTF2_GlobalDefReaderCallbacks_SetSystemTreeNodeCallback(t._global_def_callbacks,
                                                                    &callback_DefSystemTreeNode));
    OTF2_CHECK(OTF2_GlobalDefReaderCallbacks_SetLocationCallback(t._global_def_callbacks,
                                                                 &callback_DefLocation));
    OTF2_CHECK(OTF2_GlobalDefReaderCallbacks_SetLocationGroupCallback(t._global_def_callbacks,
                                                                      callback_DefLocationGroup));
    OTF2_CHECK(OTF2_GlobalDefReaderCallbacks_SetStringCallback(t._global_def_callbacks,
                                                               &callback_DefString));
    OTF2_CHECK(
            OTF2_GlobalDefReaderCallbacks_SetClockPropertiesCallback(t._global_def_callbacks,
                                                                     &callback_DefTimerResolution));
    OTF2_CHECK(OTF2_GlobalDefReaderCallbacks_SetRegionCallback(t._global_def_callbacks,
                                                               &callback_DefRegion));
    OTF2_CHECK(OTF2_GlobalDefReaderCallbacks_SetGroupCallback(t._global_def_callbacks,
                                                              &callback_DefGroup));

    OTF2_CHECK(
            OTF2_GlobalDefReaderCallbacks_SetCommCallback(t._global_def_callbacks, &callback_Comm));
    OTF2_CHECK(OTF2_GlobalDefReaderCallbacks_SetMetricMemberCallback(t._global_def_callbacks,
                                                                     &callback_DefMetricMember));
    OTF2_CHECK(OTF2_GlobalDefReaderCallbacks_SetMetricClassCallback(t._global_def_callbacks,
                                                                    &callback_DefMetricClass));
    OTF2_CHECK(
            OTF2_GlobalDefReaderCallbacks_SetMetricInstanceCallback(t._global_def_callbacks,
                                                                    &callback_DefMetricInstance));
    OTF2_CHECK(OTF2_GlobalDefReaderCallbacks_SetMetricClassRecorderCallback(
            t._global_def_callbacks,
            &callback_DefMetricClassRecorder));

    OTF2_CHECK(OTF2_Reader_RegisterGlobalDefCallbacks(t._reader,
                                                      t._global_def_reader,
                                                      t._global_def_callbacks,
                                                      &t));

    OTF2_GlobalDefReaderCallbacks_Delete(t._global_def_callbacks);

    uint64_t definitions_read = 0;
    OTF2_Reader_ReadAllGlobalDefinitions(t._reader, t._global_def_reader, &definitions_read);

    for (auto const & l : t._location) {
        OTF2_CHECK(OTF2_Reader_SelectLocation(t._reader, l.second->_location_id));
    }

    bool successful_open_def_files = OTF2_Reader_OpenDefFiles(t._reader) == OTF2_SUCCESS;
    OTF2_Reader_OpenEvtFiles(t._reader);

    for (auto const & l : t._location) {
        if (successful_open_def_files) {
            OTF2_DefReader * def_reader =
                    OTF2_Reader_GetDefReader(t._reader, l.second->_location_id);
            if (def_reader) {
                uint64_t def_reads = 0;
                OTF2_Reader_ReadAllLocalDefinitions(t._reader, def_reader, &def_reads);
                OTF2_Reader_CloseDefReader(t._reader, def_reader);
            }
        }
        // todo: remember that ?
        [[maybe_unused]] OTF2_EvtReader * evt_reader =
                OTF2_Reader_GetEvtReader(t._reader, l.second->_location_id);
    }
    if (successful_open_def_files) {
        OTF2_Reader_CloseDefFiles(t._reader);
    }

    t._global_evt_reader = OTF2_Reader_GetGlobalEvtReader(t._reader);
    t._global_evt_callbacks = OTF2_GlobalEvtReaderCallbacks_New();

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetUnknownCallback(t._global_evt_callbacks,
                                                                &callback_unknown));
    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetBufferFlushCallback(t._global_evt_callbacks,
                                                                    &callback_bufferFlush));

    OTF2_CHECK(
            OTF2_GlobalEvtReaderCallbacks_SetMeasurementOnOffCallback(t._global_evt_callbacks,
                                                                      &callback_MeasurementOnOff));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetEnterCallback(t._global_evt_callbacks,
                                                              &callback_Enter));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetLeaveCallback(t._global_evt_callbacks,
                                                              &callback_Leave));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetMpiSendCallback(t._global_evt_callbacks,
                                                                &callback_MpiSend));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetMpiIsendCallback(t._global_evt_callbacks,
                                                                 &callback_MpiIsend));

    OTF2_CHECK(
            OTF2_GlobalEvtReaderCallbacks_SetMpiIsendCompleteCallback(t._global_evt_callbacks,
                                                                      &callback_MpiIsendComplete));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetMpiIrecvRequestCallback(t._global_evt_callbacks,
                                                                        &callback_MpiIrecvRequest));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetMpiRecvCallback(t._global_evt_callbacks,
                                                                &callback_MpiRecv));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetMpiIrecvCallback(t._global_evt_callbacks,
                                                                 &callback_MpiIrecv));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetMpiRequestTestCallback(t._global_evt_callbacks,
                                                                       &callback_MpiRequestTest));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetMpiRequestCancelledCallback(
            t._global_evt_callbacks,
            &callback_MpiRequestCancelled));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetMpiCollectiveBeginCallback(
            t._global_evt_callbacks,
            &callback_MpiCollectiveBegin));

    OTF2_CHECK(
            OTF2_GlobalEvtReaderCallbacks_SetMpiCollectiveEndCallback(t._global_evt_callbacks,
                                                                      &callback_MpiCollectiveEnd));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetOmpForkCallback(t._global_evt_callbacks,
                                                                &callback_OmpFork));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetOmpJoinCallback(t._global_evt_callbacks,
                                                                &callback_OmpJoin));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetOmpAcquireLockCallback(t._global_evt_callbacks,
                                                                       &callback_OmpAcquireLock));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetOmpReleaseLockCallback(t._global_evt_callbacks,
                                                                       &callback_OmpReleaseLock));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetOmpTaskCreateCallback(t._global_evt_callbacks,
                                                                      &callback_OmpTaskCreate));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetOmpTaskSwitchCallback(t._global_evt_callbacks,
                                                                      &callback_OmpTaskSwitch));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetOmpTaskCompleteCallback(t._global_evt_callbacks,
                                                                        &callback_OmpTaskComplete));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetMetricCallback(t._global_evt_callbacks,
                                                               &callback_Metric));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetParameterStringCallback(t._global_evt_callbacks,
                                                                        &callback_ParameterString));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetParameterIntCallback(t._global_evt_callbacks,
                                                                     &callback_ParameterInt));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetParameterUnsignedIntCallback(
            t._global_evt_callbacks,
            &callback_ParameterUnsignedInt));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetRmaWinCreateCallback(t._global_evt_callbacks,
                                                                     &callback_RmaWinCreate));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetRmaWinDestroyCallback(t._global_evt_callbacks,
                                                                      &callback_RmaWinDestroy));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetRmaCollectiveBeginCallback(
            t._global_evt_callbacks,
            &callback_RmaCollectiveBegin));

    OTF2_CHECK(
            OTF2_GlobalEvtReaderCallbacks_SetRmaCollectiveEndCallback(t._global_evt_callbacks,
                                                                      &callback_RmaCollectiveEnd));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetRmaGroupSyncCallback(t._global_evt_callbacks,
                                                                     &callback_RmaGroupSync));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetRmaRequestLockCallback(t._global_evt_callbacks,
                                                                       &callback_RmaRequestLock));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetRmaAcquireLockCallback(t._global_evt_callbacks,
                                                                       &callback_RmaAcquireLock));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetRmaTryLockCallback(t._global_evt_callbacks,
                                                                   &callback_RmaTryLock));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetRmaReleaseLockCallback(t._global_evt_callbacks,
                                                                       &callback_RmaReleaseLock));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetRmaSyncCallback(t._global_evt_callbacks,
                                                                &callback_RmaSync));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetRmaWaitChangeCallback(t._global_evt_callbacks,
                                                                      &callback_RmaWaitChange));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetRmaPutCallback(t._global_evt_callbacks,
                                                               &callback_RmaPut));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetRmaGetCallback(t._global_evt_callbacks,
                                                               &callback_RmaGet));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetRmaAtomicCallback(t._global_evt_callbacks,
                                                                  &callback_RmaAtomic));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetRmaOpCompleteBlockingCallback(
            t._global_evt_callbacks,
            &callback_RmaOpCompleteBlocking));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetRmaOpCompleteNonBlockingCallback(
            t._global_evt_callbacks,
            &callback_RmaOpCompleteNonBlocking));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetRmaOpTestCallback(t._global_evt_callbacks,
                                                                  &callback_RmaOpTest));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetRmaOpCompleteRemoteCallback(
            t._global_evt_callbacks,
            &callback_RmaOpCompleteRemote));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetThreadForkCallback(t._global_evt_callbacks,
                                                                   &callback_ThreadFork));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetThreadJoinCallback(t._global_evt_callbacks,
                                                                   &callback_ThreadJoin));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetThreadTeamBeginCallback(t._global_evt_callbacks,
                                                                        &callback_ThreadTeamBegin));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetThreadTeamEndCallback(t._global_evt_callbacks,
                                                                      &callback_ThreadTeamEnd));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetThreadAcquireLockCallback(
            t._global_evt_callbacks,
            &callback_ThreadAcquireLock));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetThreadReleaseLockCallback(
            t._global_evt_callbacks,
            &callback_ThreadReleaseLock));

    OTF2_CHECK(
            OTF2_GlobalEvtReaderCallbacks_SetThreadTaskCreateCallback(t._global_evt_callbacks,
                                                                      &callback_ThreadTaskCreate));

    OTF2_CHECK(
            OTF2_GlobalEvtReaderCallbacks_SetThreadTaskSwitchCallback(t._global_evt_callbacks,
                                                                      &callback_ThreadTaskSwitch));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetThreadTaskCompleteCallback(
            t._global_evt_callbacks,
            &callback_ThreadTaskComplete));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetThreadCreateCallback(t._global_evt_callbacks,
                                                                     &callback_ThreadCreate));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetThreadBeginCallback(t._global_evt_callbacks,
                                                                    &callback_ThreadBegin));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetThreadWaitCallback(t._global_evt_callbacks,
                                                                   &callback_ThreadWait));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetThreadEndCallback(t._global_evt_callbacks,
                                                                  &callback_ThreadEnd));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetCallingContextEnterCallback(
            t._global_evt_callbacks,
            &callback_CallingContextEnter));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetCallingContextLeaveCallback(
            t._global_evt_callbacks,
            &callback_CallingContextLeave));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetCallingContextSampleCallback(
            t._global_evt_callbacks,
            &callback_CallingContextSample));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetIoCreateHandleCallback(t._global_evt_callbacks,
                                                                       &callback_IoCreateHandle));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetIoDestroyHandleCallback(t._global_evt_callbacks,
                                                                        &callback_IoDestroyHandle));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetIoDuplicateHandleCallback(
            t._global_evt_callbacks,
            &callback_IoDuplicateHandle));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetIoSeekCallback(t._global_evt_callbacks,
                                                               &callback_IoSeek));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetIoChangeStatusFlagsCallback(
            t._global_evt_callbacks,
            &callback_IoChangeStatusFlags));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetIoDeleteFileCallback(t._global_evt_callbacks,
                                                                     &callback_IoDeleteFile));

    OTF2_CHECK(
            OTF2_GlobalEvtReaderCallbacks_SetIoOperationBeginCallback(t._global_evt_callbacks,
                                                                      &callback_IoOperationBegin));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetIoOperationTestCallback(t._global_evt_callbacks,
                                                                        &callback_IoOperationTest));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetIoOperationIssuedCallback(
            t._global_evt_callbacks,
            &callback_IoOperationIssued));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetIoOperationCompleteCallback(
            t._global_evt_callbacks,
            &callback_IoOperationComplete));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetIoOperationCancelledCallback(
            t._global_evt_callbacks,
            &callback_IoOperationCancelled));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetIoAcquireLockCallback(t._global_evt_callbacks,
                                                                      &callback_IoAcquireLock));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetIoReleaseLockCallback(t._global_evt_callbacks,
                                                                      &callback_IoReleaseLock));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetIoTryLockCallback(t._global_evt_callbacks,
                                                                  &callback_IoTryLock));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetProgramBeginCallback(t._global_evt_callbacks,
                                                                     &callback_ProgramBegin));

    OTF2_CHECK(OTF2_GlobalEvtReaderCallbacks_SetProgramEndCallback(t._global_evt_callbacks,
                                                                   &callback_ProgramEnd));

    OTF2_Reader_RegisterGlobalEvtCallbacks(t._reader,
                                           t._global_evt_reader,
                                           t._global_evt_callbacks,
                                           &t);
    OTF2_GlobalEvtReaderCallbacks_Delete(t._global_evt_callbacks);
    uint64_t events_read = 0;
    OTF2_Reader_ReadAllGlobalEvents(t._reader, t._global_evt_reader, &events_read);

    OTF2_Reader_CloseGlobalEvtReader(t._reader, t._global_evt_reader);
    OTF2_Reader_CloseEvtFiles(t._reader);

    OTF2_Reader_Close(t._reader);

    for (auto p : t.sent_messages) {
        assert(p.second.empty());
    }

    //    _trace->finalize();
    return std::variant<Trace, ParsingError>(std::move(*(t._trace)));
}

OTF2_CallbackCode callback_DefTimerResolution([[maybe_unused]] void * userData,
                                              [[maybe_unused]] uint64_t timerResolution,
                                              [[maybe_unused]] uint64_t globalOffset,
                                              [[maybe_unused]] uint64_t traceLength) {
    OTF2_ENTER_CALLBACK();
    OTF2_Trace * t = (OTF2_Trace *)userData;
    t->_ticks_per_second = timerResolution;
    t->_first_timestamp = globalOffset;
    return OTF2_CALLBACK_SUCCESS;
}

OTF2_CallbackCode callback_DefString([[maybe_unused]] void * userData,
                                     [[maybe_unused]] OTF2_StringRef self,
                                     [[maybe_unused]] const char * string) {
    OTF2_ENTER_CALLBACK();
    OTF2_Trace * t = (OTF2_Trace *)userData;
    setString(t, self, std::string(string));
    assert(getString(t, self).compare(string) == 0);
    return OTF2_CALLBACK_SUCCESS;
}

// System tree callback
OTF2_CallbackCode callback_DefSystemTreeNode(
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_SystemTreeNodeRef tree_node_id,
        [[maybe_unused]] OTF2_StringRef name_id,
        [[maybe_unused]] OTF2_StringRef class_id,
        [[maybe_unused]] OTF2_SystemTreeNodeRef parent_node_id) {
    OTF2_ENTER_CALLBACK();

    OTF2_Trace * t = (OTF2_Trace *)userData;
    struct OTF2_SystemTreeNode * new_node = new struct OTF2_SystemTreeNode();
    new_node->_node_id = tree_node_id;
    new_node->_name_id = name_id;
    new_node->_parent = parent_node_id;
    setSystemTreeNode(t, tree_node_id, new_node);
    assert(getSystemTreeNode(t, tree_node_id) == new_node);

    Container * parentContainer = nullptr;
    ContainerId parent_id = ContainerId::invalid();
    OTF2_SystemTreeNode * parent_node = getSystemTreeNode(t, parent_node_id);
    if (parent_node) {
        parent_node->_child_nodes[tree_node_id] = new_node;
        parentContainer = t->_trace->container(getString(t, parent_node->_name_id));
        parent_id = parentContainer->id;
        log_debug("OTF2: New container(id={}, name={}, parent={})",
                  tree_node_id,
                  getString(t, name_id),
                  parent_node_id);
    } else {
        log_debug("OTF2: New root container(id={}, name={})", tree_node_id, getString(t, name_id));
    }

    t->_trace->createContainer(getString(t, name_id), parent_id);

    return OTF2_CALLBACK_SUCCESS;
}

OTF2_CallbackCode callback_DefLocationGroup(
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_LocationGroupRef location_group_identifier,
        [[maybe_unused]] OTF2_StringRef name,
        [[maybe_unused]] OTF2_LocationGroupType type,
        [[maybe_unused]] OTF2_SystemTreeNodeRef system_tree_parent) {
    OTF2_ENTER_CALLBACK();

    OTF2_Trace * t = (OTF2_Trace *)userData;
    struct OTF2_LocationGroup * l = new struct OTF2_LocationGroup();
    l->_group_id = location_group_identifier;
    l->_name_id = name;
    l->_node_id = system_tree_parent;
    setLocationGroup(t, location_group_identifier, l);
    struct OTF2_SystemTreeNode * parent = getSystemTreeNode(t, system_tree_parent);
    parent->_location_group[location_group_identifier] = l;

    log_debug("OTF2: New container(id={}, name={}, parent={})",
              location_group_identifier,
              getString(t, name),
              system_tree_parent);

    Container * parentContainer = nullptr;
    if (parent) {
        parentContainer = t->_trace->container(getString(t, parent->_name_id));
    }

    auto ct_name = "P#" + std::to_string(location_group_identifier);
    t->_trace->createContainer(ct_name, parentContainer->id);
    ;

    return OTF2_CALLBACK_SUCCESS;
}

OTF2_CallbackCode callback_DefLocation([[maybe_unused]] void * userData,
                                       [[maybe_unused]] OTF2_LocationRef locationIdentifier,
                                       [[maybe_unused]] OTF2_StringRef name_id,
                                       [[maybe_unused]] OTF2_LocationType location_type,
                                       [[maybe_unused]] uint64_t numberOfEvents,
                                       [[maybe_unused]] OTF2_LocationGroupRef locationGroup) {
    OTF2_ENTER_CALLBACK();

    OTF2_Trace * t = (OTF2_Trace *)userData;
    struct OTF2_Location * l = new struct OTF2_Location();
    l->_location_id = locationIdentifier;
    l->_name_id = name_id;
    l->_group_id = locationGroup;
    l->_number_of_events = numberOfEvents;

    t->_location[locationIdentifier] = l;

    struct OTF2_LocationGroup * lg = getLocationGroup(t, locationGroup);
    lg->_location[locationIdentifier] = l;

    log_debug("OTF2: New container(id={}, name={}, parent={}) {} parser->location({})",
              locationIdentifier,
              getString(t, name_id),
              locationGroup,
              l,
              locationIdentifier,
              l);

    Container * parentContainer = nullptr;
    if (lg) {
        auto parent_name = "P#" + std::to_string(locationGroup);
        parentContainer = t->_trace->container(parent_name);
    }

    auto ct_name =
            "P#" + std::to_string(locationGroup) + "_T#" + std::to_string(locationIdentifier);
    l->_container = t->_trace->createContainer(ct_name, parentContainer->id, true).id;

    return OTF2_CALLBACK_SUCCESS;
}

// Region callback
OTF2_CallbackCode callback_DefRegion([[maybe_unused]] void * userData,
                                     [[maybe_unused]] OTF2_RegionRef self,
                                     [[maybe_unused]] OTF2_StringRef name,
                                     [[maybe_unused]] OTF2_StringRef canonicalName,
                                     [[maybe_unused]] OTF2_StringRef description,
                                     [[maybe_unused]] OTF2_RegionRole regionRole,
                                     [[maybe_unused]] OTF2_Paradigm paradigm,
                                     [[maybe_unused]] OTF2_RegionFlag regionFlags,
                                     [[maybe_unused]] OTF2_StringRef sourceFile,
                                     [[maybe_unused]] uint32_t beginLineNumber,
                                     [[maybe_unused]] uint32_t endLineNumber) {
    OTF2_ENTER_CALLBACK();

    OTF2_Trace * t = (OTF2_Trace *)userData;
    struct OTF2_Function * f = new struct OTF2_Function();
    f->_region_id = self;
    f->_name_id = name;
    f->_canonicalName = canonicalName;
    f->_region_description = description;
    f->_regionRole = regionRole;
    f->_paradigm = paradigm;
    f->_regionFlags = regionFlags;
    f->_sourceFile = sourceFile;
    f->_begin_line_number = beginLineNumber;
    f->_end_line_number = endLineNumber;

    setFunction(t, self, f);

    log_debug("OTF2: New function(id={}, name={})", self, getString(t, name));

    return OTF2_CALLBACK_SUCCESS;
}

OTF2_CallbackCode callback_Comm(void * userData,
                                OTF2_CommRef self,
                                OTF2_StringRef name,
                                OTF2_GroupRef group,
                                OTF2_CommRef parent) {
    // defines an MPI communicator
    OTF2_Trace * t = (OTF2_Trace *)userData;
    struct OTF2_Comm * c = new struct OTF2_Comm();
    c->_id = self;
    c->_name = name;
    c->_group = group;
    c->_parent = parent;
    t->_comms[self] = c;
    //    setComm(t, self, c);
    log_debug("OTF2: New Communicator(id={}, name={}) {} / {}",
              self,
              getString(t, name),
              c,
              t->_comms[self]);
    return OTF2_CALLBACK_SUCCESS;
}

OTF2_CallbackCode callback_DefGroup([[maybe_unused]] void * userData,
                                    [[maybe_unused]] OTF2_GroupRef group_id,
                                    [[maybe_unused]] OTF2_StringRef name_id,
                                    [[maybe_unused]] OTF2_GroupType type,
                                    [[maybe_unused]] OTF2_Paradigm paradigm,
                                    [[maybe_unused]] OTF2_GroupFlag flags,
                                    [[maybe_unused]] uint32_t numberOfMembers,
                                    [[maybe_unused]] const uint64_t * members) {
    OTF2_ENTER_CALLBACK();

    if (type == OTF2_GROUP_TYPE_COMM_LOCATIONS) {
        OTF2_Trace * t = (OTF2_Trace *)userData;
        struct OTF2_Group * g = new struct OTF2_Group();
        g->_id = group_id;
        g->_name = name_id;
        g->_type = type;
        g->_paradigm = paradigm;
        g->_flags = flags;
        g->_members.resize(numberOfMembers);
        for (uint32_t i = 0; i < numberOfMembers; i++) {
            g->_members[i] = members[i];
        }

        setGroup(t, group_id, g);

        log_debug("OTF2: New Group(id={}, name={}", group_id, getString(t, name_id));
        for (uint32_t i = 0; i < numberOfMembers; i++) {
            log_debug("\tg[{}] = {}", i, g->_members[i]);
        }
    } else {
        switch (type) {
            case OTF2_GROUP_TYPE_UNKNOWN: break;
            case OTF2_GROUP_TYPE_LOCATIONS:
                log_debug("OTF2: New location group: group_id {}, name_id {})", group_id, name_id);
                break;
            case OTF2_GROUP_TYPE_REGIONS:
                log_debug("OTF2: New region group: group_id {}, name_id {}", group_id, name_id);
                break;
            case OTF2_GROUP_TYPE_METRIC:
                log_debug("OTF2: New metric group: group_id {}, name_id {}", group_id, name_id);
                break;
            case OTF2_GROUP_TYPE_COMM_LOCATIONS:
                log_debug("OTF2: New comm location group: group_id {}, name_id {}",
                          group_id,
                          name_id);
                break;
            case OTF2_GROUP_TYPE_COMM_GROUP:
                log_debug("OTF2: New comm group: group_id {}, name_id {}", group_id, name_id);
                break;
            case OTF2_GROUP_TYPE_COMM_SELF:
                log_debug("OTF2: New comm self: group_id {}, name_id {}", group_id, name_id);
                break;
            default: log_warning("OTF2: Type {} not yet handler", type); break;
        }
    }
    return OTF2_CALLBACK_SUCCESS;
}

OTF2_CallbackCode callback_DefMetricMember([[maybe_unused]] void * userData,
                                           [[maybe_unused]] OTF2_MetricMemberRef self,
                                           [[maybe_unused]] OTF2_StringRef name,
                                           [[maybe_unused]] OTF2_StringRef description,
                                           [[maybe_unused]] OTF2_MetricType metricType,
                                           [[maybe_unused]] OTF2_MetricMode metricMode,
                                           [[maybe_unused]] OTF2_Type valueType,
                                           [[maybe_unused]] OTF2_Base base,
                                           [[maybe_unused]] int64_t exponent,
                                           [[maybe_unused]] OTF2_StringRef unit) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

OTF2_CallbackCode callback_DefMetricClass(
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_MetricRef self,
        [[maybe_unused]] uint8_t numberOfMetrics,
        [[maybe_unused]] const OTF2_MetricMemberRef * metricMembers,
        [[maybe_unused]] OTF2_MetricOccurrence metricOccurrence,
        [[maybe_unused]] OTF2_RecorderKind recorderKind) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

OTF2_CallbackCode callback_DefMetricInstance([[maybe_unused]] void * userData,
                                             [[maybe_unused]] OTF2_MetricRef self,
                                             [[maybe_unused]] OTF2_MetricRef metricClass,
                                             [[maybe_unused]] OTF2_LocationRef recorder,
                                             [[maybe_unused]] OTF2_MetricScope metricScope,
                                             [[maybe_unused]] uint64_t scope) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

OTF2_CallbackCode callback_DefMetricClassRecorder([[maybe_unused]] void * userData,
                                                  [[maybe_unused]] OTF2_MetricRef metric,
                                                  [[maybe_unused]] OTF2_LocationRef recorder) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

OTF2_CallbackCode callback_unknown([[maybe_unused]] OTF2_LocationRef locationID,
                                   [[maybe_unused]] OTF2_TimeStamp time,
                                   [[maybe_unused]] void * userData,
                                   [[maybe_unused]] OTF2_AttributeList * attributeList) {
    OTF2_ENTER_CALLBACK();
    NOT_IMPLEMENTED();
}

OTF2_CallbackCode callback_bufferFlush([[maybe_unused]] OTF2_LocationRef locationID,
                                       [[maybe_unused]] OTF2_TimeStamp time,
                                       [[maybe_unused]] void * userData,
                                       [[maybe_unused]] OTF2_AttributeList * attributeList,
                                       [[maybe_unused]] OTF2_TimeStamp stopTime) {
    OTF2_ENTER_CALLBACK();
    NOT_IMPLEMENTED();
}

OTF2_CallbackCode callback_MeasurementOnOff([[maybe_unused]] OTF2_LocationRef locationID,
                                            [[maybe_unused]] OTF2_TimeStamp time,
                                            [[maybe_unused]] void * userData,
                                            [[maybe_unused]] OTF2_AttributeList * attributeList,
                                            [[maybe_unused]] OTF2_MeasurementMode measurementMode) {
    OTF2_ENTER_CALLBACK();
    NOT_IMPLEMENTED();
}

OTF2_CallbackCode callback_Enter([[maybe_unused]] OTF2_LocationRef locationID,
                                 [[maybe_unused]] OTF2_TimeStamp time,
                                 [[maybe_unused]] void * userData,
                                 [[maybe_unused]] OTF2_AttributeList * attributeList,
                                 [[maybe_unused]] OTF2_RegionRef region) {
    OTF2_ENTER_CALLBACK();
    OTF2_Trace * t = (OTF2_Trace *)userData;
    Date d = getTimestamp(t, time);
    struct OTF2_Function * f = getFunction(t, region);
    struct OTF2_Location * l = getLocation(t, locationID);
    assert(l != nullptr);

    auto event = Event {
        d, EventId::invalid(), {}, EventId::invalid(), eta::EventType::EnterFunction, l->_container,
        {}
    };
    // TODO: push the fields contained in the attributeList
    Field field(t->functionName, getString(t, f->_name_id));
    event.fields.emplace_back(field);
    event.fields.emplace_back(t->procRank, l->_container);

    t->_trace->push_back_event(event);
    t->enter_events[l->_container].emplace_back(event.id);

    return OTF2_CALLBACK_SUCCESS;
}

OTF2_CallbackCode callback_Leave([[maybe_unused]] OTF2_LocationRef locationID,
                                 [[maybe_unused]] OTF2_TimeStamp time,
                                 [[maybe_unused]] void * userData,
                                 [[maybe_unused]] OTF2_AttributeList * attributeList,
                                 [[maybe_unused]] OTF2_RegionRef region) {
    OTF2_ENTER_CALLBACK();

    OTF2_Trace * t = (OTF2_Trace *)userData;
    Date d = getTimestamp(t, time);
    struct OTF2_Location * l = getLocation(t, locationID);

    auto event = Event {
        d, EventId::invalid(), {}, EventId::invalid(), eta::EventType::ExitFunction, l->_container,
        {}
    };
    // TODO: push the fields contained in the attributeList
    assert(!t->enter_events[l->_container].empty());
    auto enterEventId = t->enter_events[l->_container].back();
    t->enter_events[l->_container].pop_back();

    event.fields.emplace_back(t->enterFunctionEvent, enterEventId);

    t->_trace->push_back_event(event);
    t->_trace->event(enterEventId).fields.emplace_back(t->exitFunctionEvent, event.id);

    assert(event.timestamp >= t->_trace->event(enterEventId).timestamp);

    return OTF2_CALLBACK_SUCCESS;
}

struct OTF2_Location * locationFromRank(OTF2_Trace * t,
                                        uint32_t receiver,
                                        OTF2_CommRef communicator) {
    // TODO: this function should return a locationGroup (ie. a
    // process rank) instead of a location (ie. a thread ID)
    struct OTF2_Comm * c = getComm(t, communicator);
    struct OTF2_Group * g = getGroup(t, c->_group);
    assert(g != nullptr);
    assert(receiver < g->_members.size());
    struct OTF2_Location * l = getLocation(t, g->_members[receiver]);

    return l;
}

struct OTF2_LocationGroup * locationGroupFromRank(OTF2_Trace * t,
                                                  uint32_t receiver,
                                                  OTF2_CommRef communicator) {
    // TODO: this function should return a locationGroup (ie. a
    // process rank) instead of a location (ie. a thread ID)
    struct OTF2_Comm * c = getComm(t, communicator);
    struct OTF2_Group * g = getGroup(t, c->_group);
    assert(g != nullptr);
    assert(receiver < g->_members.size());
    struct OTF2_LocationGroup * l = getLocationGroup(t, g->_members[receiver]);

    return l;
}

static OTF2_CallbackCode callback_MpiSend([[maybe_unused]] OTF2_LocationRef locationID,
                                          [[maybe_unused]] OTF2_TimeStamp time,
                                          [[maybe_unused]] void * userData,
                                          [[maybe_unused]] OTF2_AttributeList * attributeList,
                                          [[maybe_unused]] uint32_t receiver,
                                          [[maybe_unused]] OTF2_CommRef communicator,
                                          [[maybe_unused]] uint32_t msgTag,
                                          [[maybe_unused]] uint64_t msgLength) {
    OTF2_ENTER_CALLBACK();

    OTF2_Trace * t = (OTF2_Trace *)userData;
    Date d = getTimestamp(t, time);
    struct OTF2_Location * src = getLocation(t, locationID);
    struct OTF2_Location * dest = getContainerFromRank(t, communicator, receiver);

    assert(src != nullptr);
    assert(dest != nullptr);
    auto event = Event {
        d, EventId::invalid(), {}, EventId::invalid(), eta::EventType::SendMessage, src->_container,
        {}
    };
    // TODO: push the fields contained in the attributeList
    // TODO: also push srcRank

    event.fields.emplace_back(t->srcRank, src->_container);
    event.fields.emplace_back(t->destRank, dest->_container);
    event.fields.emplace_back(t->communicator, (Int)communicator);
    event.fields.emplace_back(t->msgTag, Value(msgTag));
    event.fields.emplace_back(t->msgLength, (Int)msgLength);

    auto & it_enter = *std::find_if(t->_trace->events.rbegin(),
                                    t->_trace->events.rend(),
                                    [event](auto const & e) {
                                        return event.container_id == e.container_id &&
                                               e.type == eta::EventType::EnterFunction;
                                    });

    event.happensAfter = it_enter.id;

    t->_trace->push_back_event(event);

    it_enter.happensBefore.emplace_back(event.id);

    message msg = { src->_container, dest->_container, msgTag, communicator, event.id };
    t->sent_messages[src->_container].emplace_back(msg);

    //    Event * e = new StartLinkEvent(src->_container, t, src->_container, dest->_container,
    //    LinkType(msgTag)); src->_container->addSequence(e);
    return OTF2_CALLBACK_SUCCESS;
}

static OTF2_CallbackCode callback_MpiIsend([[maybe_unused]] OTF2_LocationRef locationID,
                                           [[maybe_unused]] OTF2_TimeStamp time,
                                           [[maybe_unused]] void * userData,
                                           [[maybe_unused]] OTF2_AttributeList * attributeList,
                                           [[maybe_unused]] uint32_t receiver,
                                           [[maybe_unused]] OTF2_CommRef communicator,
                                           [[maybe_unused]] uint32_t msgTag,
                                           [[maybe_unused]] uint64_t msgLength,
                                           [[maybe_unused]] uint64_t requestID) {
    OTF2_ENTER_CALLBACK();

    OTF2_Trace * t = (OTF2_Trace *)userData;
    Date d = getTimestamp(t, time);
    struct OTF2_Location * src = getLocation(t, locationID);
    struct OTF2_Location * dest = getContainerFromRank(t, communicator, receiver);

    //    Event * e = new StartLinkEvent(src->_container, t, src->_container, dest->_container,
    //    LinkType(msgTag));
    // src->_container->addSequence(e);
    auto event = Event {
        d, EventId::invalid(), {}, EventId::invalid(), eta::EventType::SendMessage, src->_container,
        {}
    };
    // TODO: push the fields contained in the attributeList
    // TODO: also push srcRank

    event.fields.emplace_back(t->srcRank, src->_container);
    event.fields.emplace_back(t->destRank, dest->_container);
    event.fields.emplace_back(t->communicator, communicator);
    event.fields.emplace_back(t->msgTag, Value(msgTag));
    event.fields.emplace_back(t->msgLength, (Int)msgLength);

    auto & it_enter = *std::find_if(t->_trace->events.rbegin(),
                                    t->_trace->events.rend(),
                                    [event](auto const & e) {
                                        return event.container_id == e.container_id &&
                                               e.type == eta::EventType::EnterFunction;
                                    });

    event.happensAfter = it_enter.id;

    t->_trace->push_back_event(event);

    it_enter.happensBefore.emplace_back(event.id);

    message msg = { src->_container, dest->_container, msgTag, communicator, event.id };
    t->sent_messages[src->_container].emplace_back(msg);

    return OTF2_CALLBACK_SUCCESS;
}

static OTF2_CallbackCode callback_MpiIsendComplete(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] uint64_t requestID) {
    OTF2_ENTER_CALLBACK();

    // Nothing to do here
    return OTF2_CALLBACK_SUCCESS;
}

static OTF2_CallbackCode callback_MpiIrecvRequest(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] uint64_t requestID) {
    OTF2_ENTER_CALLBACK();

    // Nothing to do here
    return OTF2_CALLBACK_SUCCESS;
}

static OTF2_CallbackCode callback_MpiRecv([[maybe_unused]] OTF2_LocationRef locationID,
                                          [[maybe_unused]] OTF2_TimeStamp time,
                                          [[maybe_unused]] void * userData,
                                          [[maybe_unused]] OTF2_AttributeList * attributeList,
                                          [[maybe_unused]] uint32_t sender,
                                          [[maybe_unused]] OTF2_CommRef communicator,
                                          [[maybe_unused]] uint32_t msgTag,
                                          [[maybe_unused]] uint64_t msgLength) {
    OTF2_ENTER_CALLBACK();

    OTF2_Trace * t = (OTF2_Trace *)userData;
    Date d = getTimestamp(t, time);
    struct OTF2_Location * dest = getLocation(t, locationID);
    struct OTF2_Location * src = getContainerFromRank(t, communicator, sender);

    //    Event * e = new StartLinkEvent(dest->_container, t, src->_container, dest->_container,
    //    LinkType(msgTag));
    // dest->_container->addSequence(e);

    auto sendMsgIt = std::find_if(t->sent_messages[src->_container].begin(),
                                  t->sent_messages[src->_container].end(),
                                  [src, dest, msgTag, communicator](message m) {
                                      return m.src == src->_container &&
                                             m.dst == dest->_container && m.tag == msgTag &&
                                             m.comm == communicator;
                                  });

    assert(sendMsgIt != t->sent_messages[src->_container].end());

    auto sendMsg = *sendMsgIt;
    t->sent_messages[src->_container].erase(sendMsgIt);

    auto event = Event { d,
                         EventId::invalid(),
                         {},
                         EventId::invalid(),
                         eta::EventType::ReceiveMessage,
                         dest->_container,
                         {} };
    // TODO: push the fields contained in the attributeList
    event.fields.emplace_back(t->srcRank, src->_container);
    event.fields.emplace_back(t->destRank, dest->_container);
    event.fields.emplace_back(t->communicator, communicator);
    event.fields.emplace_back(t->msgTag, Value(msgTag));
    event.fields.emplace_back(t->msgLength, (Int)msgLength);
    event.happensAfter = sendMsg.event;

    t->_trace->push_back_event(event);

    assert(event.timestamp >= t->_trace->event(sendMsg.event).timestamp);

    Communication c = {
        src->_container,         // src
        { dest->_container },    // dests
        (Int)msgLength,          // len
        (Int)msgTag,             // tag
        { sendMsg.event },       // std::vector<EventId> from;
        { event.id },            // std::vector<EventId> to;
        CommunicationType::P2P,  // CommunicationType type;
    };

    t->_trace->communications.emplace_back(c);

    t->_trace->event(sendMsg.event).happensBefore.emplace_back(event.id);

    return OTF2_CALLBACK_SUCCESS;
}

static OTF2_CallbackCode callback_MpiIrecv([[maybe_unused]] OTF2_LocationRef locationID,
                                           [[maybe_unused]] OTF2_TimeStamp time,
                                           [[maybe_unused]] void * userData,
                                           [[maybe_unused]] OTF2_AttributeList * attributeList,
                                           [[maybe_unused]] uint32_t sender,
                                           [[maybe_unused]] OTF2_CommRef communicator,
                                           [[maybe_unused]] uint32_t msgTag,
                                           [[maybe_unused]] uint64_t msgLength,
                                           [[maybe_unused]] uint64_t requestID) {
    OTF2_ENTER_CALLBACK();

    OTF2_Trace * t = (OTF2_Trace *)userData;
    Date d = getTimestamp(t, time);
    struct OTF2_Location * dest = getLocation(t, locationID);
    struct OTF2_Location * src = getContainerFromRank(t, communicator, sender);

    //    Event * e = new StartLinkEvent(dest->_container, t, src->_container, dest->_container,
    //    LinkType(msgTag)); dest->_container->addSequence(e);

    auto sendMsgIt = std::find_if(t->sent_messages[src->_container].begin(),
                                  t->sent_messages[src->_container].end(),
                                  [src, dest, msgTag, communicator](message m) {
                                      return m.src == src->_container &&
                                             m.dst == dest->_container && m.tag == msgTag &&
                                             m.comm == communicator;
                                  });

    assert(sendMsgIt != t->sent_messages[src->_container].end());

    auto sendMsg = *sendMsgIt;
    t->sent_messages[src->_container].erase(sendMsgIt);

    auto event = Event { d,
                         EventId::invalid(),
                         {},
                         EventId::invalid(),
                         eta::EventType::ReceiveMessage,
                         dest->_container,
                         {} };
    // TODO: push the fields contained in the attributeList
    event.fields.emplace_back(t->srcRank, src->_container);
    event.fields.emplace_back(t->destRank, dest->_container);
    event.fields.emplace_back(t->communicator, communicator);
    event.fields.emplace_back(t->msgTag, Value(msgTag));
    event.fields.emplace_back(t->msgLength, (Int)msgLength);
    event.happensAfter = sendMsg.event;

    t->_trace->push_back_event(event);

    assert(event.timestamp >= t->_trace->event(sendMsg.event).timestamp);

    Communication c = {
        src->_container,         // src
        { dest->_container },    // dests
        (Int)msgLength,          // len
        (Int)msgTag,             // tag
        { sendMsg.event },       // std::vector<EventId> from;
        { event.id },            // std::vector<EventId> to;
        CommunicationType::P2P,  // CommunicationType type;
    };

    t->_trace->communications.emplace_back(c);

    t->_trace->event(sendMsg.event).happensBefore.emplace_back(event.id);

    return OTF2_CALLBACK_SUCCESS;
}

static OTF2_CallbackCode callback_MpiRequestTest(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] uint64_t requestID) {
    OTF2_ENTER_CALLBACK();

    // Nothing to do here
    return OTF2_CALLBACK_SUCCESS;
}

static OTF2_CallbackCode callback_MpiRequestCancelled(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] uint64_t requestID) {
    OTF2_ENTER_CALLBACK();

    // Nothing to do here
    return OTF2_CALLBACK_SUCCESS;
}

static OTF2_CallbackCode callback_MpiCollectiveBegin(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList) {
    OTF2_ENTER_CALLBACK();

    OTF2_Trace * t = (OTF2_Trace *)userData;
    Date d = getTimestamp(t, time);
    struct OTF2_Location * proc = getLocation(t, locationID);

    auto event = Event { d,
                         EventId::invalid(),
                         {},
                         EventId::invalid(),
                         eta::EventType::MpiOperationBegin,
                         proc->_container,
                         {} };

    event.fields.emplace_back(t->procRank, proc->_container);

    auto & it_enter = *std::find_if(t->_trace->events.rbegin(),
                                    t->_trace->events.rend(),
                                    [event](auto const & e) {
                                        return event.container_id == e.container_id &&
                                               e.type == eta::EventType::EnterFunction;
                                    });

    event.happensAfter = it_enter.id;

    t->_trace->push_back_event(event);

    it_enter.happensBefore.emplace_back(event.id);

    t->mpi_events[proc->_container].emplace_back(event.id);

    return OTF2_CALLBACK_SUCCESS;
}

static OTF2_CallbackCode callback_MpiCollectiveEnd(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] OTF2_CollectiveOp collectiveOp,
        [[maybe_unused]] OTF2_CommRef communicator,
        [[maybe_unused]] uint32_t root,
        [[maybe_unused]] uint64_t sizeSent,
        [[maybe_unused]] uint64_t sizeReceived) {
    OTF2_ENTER_CALLBACK();

    OTF2_Trace * t = (OTF2_Trace *)userData;
    Date d = getTimestamp(t, time);
    struct OTF2_Location * procRank = getLocation(t, locationID);
    auto rootId = eta::ContainerId::invalid();
    auto event = Event { d,
                         EventId::invalid(),
                         {},
                         EventId::invalid(),
                         eta::EventType::MpiOperationComplete,
                         procRank->_container,
                         {} };

    auto enterEventId = t->mpi_events[procRank->_container].back();
    t->mpi_events[procRank->_container].pop_back();
    event.fields.emplace_back(t->enterFunctionEvent, enterEventId);

    if (root != OTF2_UNDEFINED_UINT32) {
        // warning root could be undefined, e.g. for BARRIER and ALLREDUCE events
        struct OTF2_Location * rootRank = locationFromRank(t, root, communicator);
        event.fields.emplace_back(t->rootRank, rootRank->_container);
        rootId = rootRank->_container;
        t->_trace->event(enterEventId).fields.emplace_back(t->rootRank, rootRank->_container);
    }

    event.fields.emplace_back(t->procRank, procRank->_container);
    event.fields.emplace_back(t->communicator, (Int)communicator);
    if (sizeSent != sizeReceived) {
        log_warning("OTF2: Warning : sizeSent != sizeReceived");
    }
    event.fields.emplace_back(t->msgLength, (Int)sizeSent);
    event.happensAfter = enterEventId;

    t->_trace->push_back_event(event);

    t->_trace->event(enterEventId).happensBefore.emplace_back(event.id);
    t->_trace->event(enterEventId).fields.emplace_back(t->mpiEvent, event.id);

    auto type = eta::to_communication_type(collectiveOp);
    auto find = 0;
    auto comm_complete = false;

    for (auto & com : t->current_coll_comm) {
        if (com.type == type && com.src == rootId && com.len == (Int)sizeSent) {
            auto it = std::find(com.dests.begin(), com.dests.end(), procRank->_container);
            if (it == com.dests.end()) {
                com.dests.push_back(procRank->_container);
                com.from.push_back(enterEventId);
                com.to.push_back(event.id);

                auto comm_members = t->_groups[t->_comms[communicator]->_group]->_members;

                if (comm_members.size() == com.dests.size()) {
                    comm_complete = true;
                }

                if (comm_complete) {
                    t->_trace->communications.emplace_back(com);
                }
                find = 1;
                break;
            }
        }
    }

    if (find && comm_complete) {
        int n = 0;
        auto const & com = t->_trace->communications.back();

        for (int i = 0; i < t->current_coll_comm.size(); ++i) {
            if (t->current_coll_comm[i].dests == com.dests &&
                t->current_coll_comm[i].from == com.from &&
                t->current_coll_comm[i].src == com.src && t->current_coll_comm[i].len == com.len &&
                t->current_coll_comm[i].to == com.to && t->current_coll_comm[i].type == com.type) {
                n = i;
            }
        }

        t->current_coll_comm.erase(t->current_coll_comm.begin() + n);
    }

    if (!find) {
        Communication c = {
            rootId,                    // src
            { procRank->_container },  // dests
            (Int)sizeSent,             // length
            0,                         // tag
            { enterEventId },          // std::vector<EventId> from;
            { event.id },              // std::vector<EventId> to;
            type,                      // CommunicationType type;
        };
        t->current_coll_comm.emplace_back(c);
    }

    return OTF2_CALLBACK_SUCCESS;
}

static OTF2_CallbackCode callback_OmpFork([[maybe_unused]] OTF2_LocationRef locationID,
                                          [[maybe_unused]] OTF2_TimeStamp time,
                                          [[maybe_unused]] void * userData,
                                          [[maybe_unused]] OTF2_AttributeList * attributeList,
                                          [[maybe_unused]] uint32_t numberOfRequestedThreads) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_OmpJoin([[maybe_unused]] OTF2_LocationRef locationID,
                                          [[maybe_unused]] OTF2_TimeStamp time,
                                          [[maybe_unused]] void * userData,
                                          [[maybe_unused]] OTF2_AttributeList * attributeList) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_OmpAcquireLock(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] uint32_t lockID,
        [[maybe_unused]] uint32_t acquisitionOrder) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_OmpReleaseLock(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] uint32_t lockID,
        [[maybe_unused]] uint32_t acquisitionOrder) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_OmpTaskCreate([[maybe_unused]] OTF2_LocationRef locationID,
                                                [[maybe_unused]] OTF2_TimeStamp time,
                                                [[maybe_unused]] void * userData,
                                                [[maybe_unused]] OTF2_AttributeList * attributeList,
                                                [[maybe_unused]] uint64_t taskID) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_OmpTaskSwitch([[maybe_unused]] OTF2_LocationRef locationID,
                                                [[maybe_unused]] OTF2_TimeStamp time,
                                                [[maybe_unused]] void * userData,
                                                [[maybe_unused]] OTF2_AttributeList * attributeList,
                                                [[maybe_unused]] uint64_t taskID) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_OmpTaskComplete(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] uint64_t taskID) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_Metric([[maybe_unused]] OTF2_LocationRef locationID,
                                         [[maybe_unused]] OTF2_TimeStamp time,
                                         [[maybe_unused]] void * userData,
                                         [[maybe_unused]] OTF2_AttributeList * attributeList,
                                         [[maybe_unused]] OTF2_MetricRef metric,
                                         [[maybe_unused]] uint8_t numberOfMetrics,
                                         [[maybe_unused]] const OTF2_Type * typeIDs,
                                         [[maybe_unused]] const OTF2_MetricValue * metricValues) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_ParameterString(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] OTF2_ParameterRef parameter,
        [[maybe_unused]] OTF2_StringRef string) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_ParameterInt([[maybe_unused]] OTF2_LocationRef locationID,
                                               [[maybe_unused]] OTF2_TimeStamp time,
                                               [[maybe_unused]] void * userData,
                                               [[maybe_unused]] OTF2_AttributeList * attributeList,
                                               [[maybe_unused]] OTF2_ParameterRef parameter,
                                               [[maybe_unused]] int64_t value) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_ParameterUnsignedInt(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] OTF2_ParameterRef parameter,
        [[maybe_unused]] uint64_t value) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_RmaWinCreate([[maybe_unused]] OTF2_LocationRef locationID,
                                               [[maybe_unused]] OTF2_TimeStamp time,
                                               [[maybe_unused]] void * userData,
                                               [[maybe_unused]] OTF2_AttributeList * attributeList,
                                               [[maybe_unused]] OTF2_RmaWinRef win) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_RmaWinDestroy([[maybe_unused]] OTF2_LocationRef locationID,
                                                [[maybe_unused]] OTF2_TimeStamp time,
                                                [[maybe_unused]] void * userData,
                                                [[maybe_unused]] OTF2_AttributeList * attributeList,
                                                [[maybe_unused]] OTF2_RmaWinRef win) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_RmaCollectiveBegin(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_RmaCollectiveEnd(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] OTF2_CollectiveOp collectiveOp,
        [[maybe_unused]] OTF2_RmaSyncLevel syncLevel,
        [[maybe_unused]] OTF2_RmaWinRef win,
        [[maybe_unused]] uint32_t root,
        [[maybe_unused]] uint64_t bytesSent,
        [[maybe_unused]] uint64_t bytesReceived) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_RmaGroupSync([[maybe_unused]] OTF2_LocationRef locationID,
                                               [[maybe_unused]] OTF2_TimeStamp time,
                                               [[maybe_unused]] void * userData,
                                               [[maybe_unused]] OTF2_AttributeList * attributeList,
                                               [[maybe_unused]] OTF2_RmaSyncLevel syncLevel,
                                               [[maybe_unused]] OTF2_RmaWinRef win,
                                               [[maybe_unused]] OTF2_GroupRef group) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_RmaRequestLock(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] OTF2_RmaWinRef win,
        [[maybe_unused]] uint32_t remote,
        [[maybe_unused]] uint64_t lockId,
        [[maybe_unused]] OTF2_LockType lockType) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_RmaAcquireLock(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] OTF2_RmaWinRef win,
        [[maybe_unused]] uint32_t remote,
        [[maybe_unused]] uint64_t lockId,
        [[maybe_unused]] OTF2_LockType lockType) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_RmaTryLock([[maybe_unused]] OTF2_LocationRef locationID,
                                             [[maybe_unused]] OTF2_TimeStamp time,
                                             [[maybe_unused]] void * userData,
                                             [[maybe_unused]] OTF2_AttributeList * attributeList,
                                             [[maybe_unused]] OTF2_RmaWinRef win,
                                             [[maybe_unused]] uint32_t remote,
                                             [[maybe_unused]] uint64_t lockId,
                                             [[maybe_unused]] OTF2_LockType lockType) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_RmaReleaseLock(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] OTF2_RmaWinRef win,
        [[maybe_unused]] uint32_t remote,
        [[maybe_unused]] uint64_t lockId) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_RmaSync([[maybe_unused]] OTF2_LocationRef locationID,
                                          [[maybe_unused]] OTF2_TimeStamp time,
                                          [[maybe_unused]] void * userData,
                                          [[maybe_unused]] OTF2_AttributeList * attributeList,
                                          [[maybe_unused]] OTF2_RmaWinRef win,
                                          [[maybe_unused]] uint32_t remote,
                                          [[maybe_unused]] OTF2_RmaSyncType syncType) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_RmaWaitChange([[maybe_unused]] OTF2_LocationRef locationID,
                                                [[maybe_unused]] OTF2_TimeStamp time,
                                                [[maybe_unused]] void * userData,
                                                [[maybe_unused]] OTF2_AttributeList * attributeList,
                                                [[maybe_unused]] OTF2_RmaWinRef win) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_RmaPut([[maybe_unused]] OTF2_LocationRef locationID,
                                         [[maybe_unused]] OTF2_TimeStamp time,
                                         [[maybe_unused]] void * userData,
                                         [[maybe_unused]] OTF2_AttributeList * attributeList,
                                         [[maybe_unused]] OTF2_RmaWinRef win,
                                         [[maybe_unused]] uint32_t remote,
                                         [[maybe_unused]] uint64_t bytes,
                                         [[maybe_unused]] uint64_t matchingId) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_RmaGet([[maybe_unused]] OTF2_LocationRef locationID,
                                         [[maybe_unused]] OTF2_TimeStamp time,
                                         [[maybe_unused]] void * userData,
                                         [[maybe_unused]] OTF2_AttributeList * attributeList,
                                         [[maybe_unused]] OTF2_RmaWinRef win,
                                         [[maybe_unused]] uint32_t remote,
                                         [[maybe_unused]] uint64_t bytes,
                                         [[maybe_unused]] uint64_t matchingId) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_RmaAtomic([[maybe_unused]] OTF2_LocationRef locationID,
                                            [[maybe_unused]] OTF2_TimeStamp time,
                                            [[maybe_unused]] void * userData,
                                            [[maybe_unused]] OTF2_AttributeList * attributeList,
                                            [[maybe_unused]] OTF2_RmaWinRef win,
                                            [[maybe_unused]] uint32_t remote,
                                            [[maybe_unused]] OTF2_RmaAtomicType type,
                                            [[maybe_unused]] uint64_t bytesSent,
                                            [[maybe_unused]] uint64_t bytesReceived,
                                            [[maybe_unused]] uint64_t matchingId) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_RmaOpCompleteBlocking(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] OTF2_RmaWinRef win,
        [[maybe_unused]] uint64_t matchingId) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_RmaOpCompleteNonBlocking(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] OTF2_RmaWinRef win,
        [[maybe_unused]] uint64_t matchingId) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_RmaOpTest([[maybe_unused]] OTF2_LocationRef locationID,
                                            [[maybe_unused]] OTF2_TimeStamp time,
                                            [[maybe_unused]] void * userData,
                                            [[maybe_unused]] OTF2_AttributeList * attributeList,
                                            [[maybe_unused]] OTF2_RmaWinRef win,
                                            [[maybe_unused]] uint64_t matchingId) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_RmaOpCompleteRemote(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] OTF2_RmaWinRef win,
        [[maybe_unused]] uint64_t matchingId) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_ThreadFork([[maybe_unused]] OTF2_LocationRef locationID,
                                             [[maybe_unused]] OTF2_TimeStamp time,
                                             [[maybe_unused]] void * userData,
                                             [[maybe_unused]] OTF2_AttributeList * attributeList,
                                             [[maybe_unused]] OTF2_Paradigm model,
                                             [[maybe_unused]] uint32_t numberOfRequestedThreads) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_ThreadJoin([[maybe_unused]] OTF2_LocationRef locationID,
                                             [[maybe_unused]] OTF2_TimeStamp time,
                                             [[maybe_unused]] void * userData,
                                             [[maybe_unused]] OTF2_AttributeList * attributeList,
                                             [[maybe_unused]] OTF2_Paradigm model) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_ThreadTeamBegin(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] OTF2_CommRef threadTeam) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_ThreadTeamEnd([[maybe_unused]] OTF2_LocationRef locationID,
                                                [[maybe_unused]] OTF2_TimeStamp time,
                                                [[maybe_unused]] void * userData,
                                                [[maybe_unused]] OTF2_AttributeList * attributeList,
                                                [[maybe_unused]] OTF2_CommRef threadTeam) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_ThreadAcquireLock(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] OTF2_Paradigm model,
        [[maybe_unused]] uint32_t lockID,
        [[maybe_unused]] uint32_t acquisitionOrder) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_ThreadReleaseLock(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] OTF2_Paradigm model,
        [[maybe_unused]] uint32_t lockID,
        [[maybe_unused]] uint32_t acquisitionOrder) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_ThreadTaskCreate(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] OTF2_CommRef threadTeam,
        [[maybe_unused]] uint32_t creatingThread,
        [[maybe_unused]] uint32_t generationNumber) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_ThreadTaskSwitch(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] OTF2_CommRef threadTeam,
        [[maybe_unused]] uint32_t creatingThread,
        [[maybe_unused]] uint32_t generationNumber) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_ThreadTaskComplete(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] OTF2_CommRef threadTeam,
        [[maybe_unused]] uint32_t creatingThread,
        [[maybe_unused]] uint32_t generationNumber) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_ThreadCreate([[maybe_unused]] OTF2_LocationRef locationID,
                                               [[maybe_unused]] OTF2_TimeStamp time,
                                               [[maybe_unused]] void * userData,
                                               [[maybe_unused]] OTF2_AttributeList * attributeList,
                                               [[maybe_unused]] OTF2_CommRef threadContingent,
                                               [[maybe_unused]] uint64_t sequenceCount) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_ThreadBegin([[maybe_unused]] OTF2_LocationRef locationID,
                                              [[maybe_unused]] OTF2_TimeStamp time,
                                              [[maybe_unused]] void * userData,
                                              [[maybe_unused]] OTF2_AttributeList * attributeList,
                                              [[maybe_unused]] OTF2_CommRef threadContingent,
                                              [[maybe_unused]] uint64_t sequenceCount) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_ThreadWait([[maybe_unused]] OTF2_LocationRef locationID,
                                             [[maybe_unused]] OTF2_TimeStamp time,
                                             [[maybe_unused]] void * userData,
                                             [[maybe_unused]] OTF2_AttributeList * attributeList,
                                             [[maybe_unused]] OTF2_CommRef threadContingent,
                                             [[maybe_unused]] uint64_t sequenceCount) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_ThreadEnd([[maybe_unused]] OTF2_LocationRef locationID,
                                            [[maybe_unused]] OTF2_TimeStamp time,
                                            [[maybe_unused]] void * userData,
                                            [[maybe_unused]] OTF2_AttributeList * attributeList,
                                            [[maybe_unused]] OTF2_CommRef threadContingent,
                                            [[maybe_unused]] uint64_t sequenceCount) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_CallingContextEnter(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] OTF2_CallingContextRef callingContext,
        [[maybe_unused]] uint32_t unwindDistance) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_CallingContextLeave(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] OTF2_CallingContextRef callingContext) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_CallingContextSample(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] OTF2_CallingContextRef callingContext,
        [[maybe_unused]] uint32_t unwindDistance,
        [[maybe_unused]] OTF2_InterruptGeneratorRef interruptGenerator) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_IoCreateHandle(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] OTF2_IoHandleRef handle,
        [[maybe_unused]] OTF2_IoAccessMode mode,
        [[maybe_unused]] OTF2_IoCreationFlag creationFlags,
        [[maybe_unused]] OTF2_IoStatusFlag statusFlags) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_IoDestroyHandle(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] OTF2_IoHandleRef handle) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_IoDuplicateHandle(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] OTF2_IoHandleRef oldHandle,
        [[maybe_unused]] OTF2_IoHandleRef newHandle,
        [[maybe_unused]] OTF2_IoStatusFlag statusFlags) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_IoSeek([[maybe_unused]] OTF2_LocationRef locationID,
                                         [[maybe_unused]] OTF2_TimeStamp time,
                                         [[maybe_unused]] void * userData,
                                         [[maybe_unused]] OTF2_AttributeList * attributeList,
                                         [[maybe_unused]] OTF2_IoHandleRef handle,
                                         [[maybe_unused]] int64_t offsetRequest,
                                         [[maybe_unused]] OTF2_IoSeekOption whence,
                                         [[maybe_unused]] uint64_t offsetResult) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_IoChangeStatusFlags(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] OTF2_IoHandleRef handle,
        [[maybe_unused]] OTF2_IoStatusFlag statusFlags) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_IoDeleteFile([[maybe_unused]] OTF2_LocationRef locationID,
                                               [[maybe_unused]] OTF2_TimeStamp time,
                                               [[maybe_unused]] void * userData,
                                               [[maybe_unused]] OTF2_AttributeList * attributeList,
                                               [[maybe_unused]] OTF2_IoParadigmRef ioParadigm,
                                               [[maybe_unused]] OTF2_IoFileRef file) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_IoOperationBegin(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] OTF2_IoHandleRef handle,
        [[maybe_unused]] OTF2_IoOperationMode mode,
        [[maybe_unused]] OTF2_IoOperationFlag operationFlags,
        [[maybe_unused]] uint64_t bytesRequest,
        [[maybe_unused]] uint64_t matchingId) {
    OTF2_ENTER_CALLBACK();

    OTF2_Trace * t = (OTF2_Trace *)userData;
    Date d = getTimestamp(t, time);
    struct OTF2_Location * src = getLocation(t, locationID);

    auto event = Event { d,
                         EventId::invalid(),
                         {},
                         EventId::invalid(),
                         eta::EventType::IoOperationBegin,
                         src->_container,
                         {} };

    event.fields.emplace_back(t->msgLength, (Int)bytesRequest);
    event.fields.emplace_back(t->matchingId, (Int)matchingId);
    event.fields.emplace_back(t->iOMode, (Int)mode);

    t->_trace->push_back_event(event);

    t->posixio_events[src->_container].emplace_back(event.id);
    return OTF2_CALLBACK_SUCCESS;
}

static OTF2_CallbackCode callback_IoOperationTest(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] OTF2_IoHandleRef handle,
        [[maybe_unused]] uint64_t matchingId) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_IoOperationIssued(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] OTF2_IoHandleRef handle,
        [[maybe_unused]] uint64_t matchingId) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_IoOperationComplete(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] OTF2_IoHandleRef handle,
        [[maybe_unused]] uint64_t bytesResult,
        [[maybe_unused]] uint64_t matchingId) {
    OTF2_ENTER_CALLBACK();

    OTF2_Trace * t = (OTF2_Trace *)userData;
    Date d = getTimestamp(t, time);
    struct OTF2_Location * src = getLocation(t, locationID);

    auto event = Event { d,
                         EventId::invalid(),
                         {},
                         EventId::invalid(),
                         eta::EventType::IoOperationComplete,
                         src->_container,
                         {} };

    event.fields.emplace_back(t->msgLength, (Int)bytesResult);
    event.fields.emplace_back(t->matchingId, (Int)matchingId);

    t->_trace->push_back_event(event);

    auto enterEventId = t->posixio_events[src->_container].back();
    /* Check if both matching ids are equal */
    assert(std::get<Int>(get_field(t->_trace->event(enterEventId), t->matchingId).value()) ==
           (Int)matchingId);

    t->posixio_events[src->_container].pop_back();
    event.fields.emplace_back(t->enterFunctionEvent, enterEventId);
    t->_trace->event(enterEventId).fields.emplace_back(t->exitFunctionEvent, event.id);

    return OTF2_CALLBACK_SUCCESS;
}

static OTF2_CallbackCode callback_IoOperationCancelled(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] OTF2_IoHandleRef handle,
        [[maybe_unused]] uint64_t matchingId) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_IoAcquireLock([[maybe_unused]] OTF2_LocationRef locationID,
                                                [[maybe_unused]] OTF2_TimeStamp time,
                                                [[maybe_unused]] void * userData,
                                                [[maybe_unused]] OTF2_AttributeList * attributeList,
                                                [[maybe_unused]] OTF2_IoHandleRef handle,
                                                [[maybe_unused]] OTF2_LockType lockType) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_IoReleaseLock([[maybe_unused]] OTF2_LocationRef locationID,
                                                [[maybe_unused]] OTF2_TimeStamp time,
                                                [[maybe_unused]] void * userData,
                                                [[maybe_unused]] OTF2_AttributeList * attributeList,
                                                [[maybe_unused]] OTF2_IoHandleRef handle,
                                                [[maybe_unused]] OTF2_LockType lockType) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_IoTryLock([[maybe_unused]] OTF2_LocationRef locationID,
                                            [[maybe_unused]] OTF2_TimeStamp time,
                                            [[maybe_unused]] void * userData,
                                            [[maybe_unused]] OTF2_AttributeList * attributeList,
                                            [[maybe_unused]] OTF2_IoHandleRef handle,
                                            [[maybe_unused]] OTF2_LockType lockType) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_ProgramBegin(
        [[maybe_unused]] OTF2_LocationRef locationID,
        [[maybe_unused]] OTF2_TimeStamp time,
        [[maybe_unused]] void * userData,
        [[maybe_unused]] OTF2_AttributeList * attributeList,
        [[maybe_unused]] OTF2_StringRef programName,
        [[maybe_unused]] uint32_t numberOfArguments,
        [[maybe_unused]] const OTF2_StringRef * programArguments) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

static OTF2_CallbackCode callback_ProgramEnd([[maybe_unused]] OTF2_LocationRef locationID,
                                             [[maybe_unused]] OTF2_TimeStamp time,
                                             [[maybe_unused]] void * userData,
                                             [[maybe_unused]] OTF2_AttributeList * attributeList,
                                             [[maybe_unused]] int64_t exitStatus) {
    OTF2_ENTER_CALLBACK();

    NOT_IMPLEMENTED();
}

}  // namespace eta::otf2
