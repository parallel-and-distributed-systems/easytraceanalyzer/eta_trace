#include "loader.hpp"

#include <eta/core/format.hpp>

#include "ltrace.hpp"
#include "otf2.hpp"
#include "paje.hpp"

namespace eta {

auto evaluateTraceType(Path const & path) -> TraceType {
#ifdef ENABLE_PAJE
    if (paje::accept(path))
        return TraceType::Paje;
#endif
#ifdef ENABLE_LTRACE
    if (ltrace::accept(path))
        return TraceType::LTrace;
#endif
#ifdef ENABLE_OTF2
    if (otf2::accept(path))
        return TraceType::Otf2;
#endif
#ifdef ENABLE_DARSHAN
    if (darshan::accept(path))
        return TraceType::Darshan;
#endif
    Assert(false, "Trace type not supported (", path.c_str(), ")");
    return TraceType::Auto;
}

auto loadTrace(Path const & path, TraceType type) -> std::variant<Trace, ParsingError> {
    if (type == TraceType::Auto)
        type = evaluateTraceType(path);

    switch (type) {
#ifdef ENABLE_PAJE
        case TraceType::Paje: return paje::loadTrace(path);
#endif
#ifdef ENABLE_LTRACE
        case TraceType::LTrace: return ltrace::loadTrace(path);
#endif
#ifdef ENABLE_OTF2
        case TraceType::Otf2: return otf2::loadTrace(path);
#endif
#ifdef ENABLE_DARSHAN
        case Tracetype::Darshan: return darshan::loadTrace(path);
#endif
        default: {
            auto err = ParsingError {
                eta::format("Unable to deduce trace format for file \"", path.c_str(), '"')
            };
            err.file = path;
            return err;
        };
    }
}

}  // namespace eta
