#pragma once

namespace eta {

#define FORWARD_SIMPLE_ERROR(err)          \
    do {                                   \
        if (err.has_value()) {             \
            return std::move(err).value(); \
        }                                  \
    } while (false)

#define CHECK_SIMPLE_ERROR(err)            \
    do {                                   \
        if (err.has_value()) {             \
            err->line = line_number;       \
            err->file = path;              \
            return std::move(err).value(); \
        }                                  \
    } while (false)

#define CHECK_ERROR(err)                                         \
    do {                                                         \
        if (std::holds_alternative<ParsingError>(err)) {         \
            auto error = std::get<ParsingError>(std::move(err)); \
            error.line = line_number;                            \
            error.file = path;                                   \
            return error;                                        \
        }                                                        \
    } while (false)

#define FORWARD_ERROR(err)                                 \
    do {                                                   \
        if (std::holds_alternative<ParsingError>(err)) {   \
            return std::get<ParsingError>(std::move(err)); \
        }                                                  \
    } while (false)

#define CHECK_LINE_ENDED(src, index)                             \
    do {                                                         \
        consumeSpaces(line, index);                              \
        if (index != line.size())                                \
            return ParsingError { "unexpected extra characters", \
                                  src,                           \
                                  index,                         \
                                  line.size() - index };         \
    } while (false)

}  // namespace eta
