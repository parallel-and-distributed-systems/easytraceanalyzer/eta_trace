#pragma once

#include <cstdlib>
#include <eta/core/format.hpp>

#include "definitions.hpp"
#include "parsing_error.hpp"

namespace eta {

[[nodiscard]] inline auto isLetter(char c) {
    return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c == '_');
}
[[nodiscard]] static auto isDigit(char c) { return c >= '0' && c <= '9'; }
[[nodiscard]] static auto isWhiteSpace(char c) { return c == ' ' || c == '\t' || c == '\n'; }

inline auto consumeSpaces(std::string_view const & src, std::size_t & i) {
    auto const end = src.size();
    while (i != end && (src[i] == ' ' || src[i] == '\t'))
        ++i;
}

inline auto consumeToken(std::string_view const & src, std::size_t & i) {
    auto const end = src.size();
    while (i != end && (isLetter(src[i]) || isDigit(src[i])))
        ++i;
}

template <auto size>
[[nodiscard]] inline auto consume(std::string_view const & src,
                                  std::size_t & begin,
                                  char const (&str)[size]) -> std::optional<ParsingError> {
    auto const end = src.size();
    auto i = 0u;
    auto j = begin;

    while (true) {
        if (i == size - 1) {  // ignore null final char
            begin = j;
            return std::nullopt;
        }

        if (j == end) {
            return ParsingError { eta::format("Unexpected end of line in ",
                                              src.substr(begin),
                                              " while searching for ",
                                              str),
                                  src,
                                  begin,
                                  end - begin

            };
        }

        if (src[j] != str[i]) {
            consumeToken(str, j);
            return ParsingError { eta::format("Token ",
                                              src.substr(begin, j - begin),
                                              "was found when ",
                                              str,
                                              "was expected"),
                                  src,
                                  begin,
                                  j - begin

            };
        }

        ++i;
        ++j;
    }
}

[[nodiscard]] inline auto consumeInt(std::string_view const & str, std::size_t & begin)
        -> std::variant<Int, ParsingError> {
    if (str.size() <= begin) {
        return ParsingError { eta::format("Unexpected end of line"), str, begin, 0 };
    }
    errno = 0;
    auto it = static_cast<char const *>(str.begin());
    auto const res = std::strtol(str.begin() + begin, const_cast<char **>(&it), 10);
    auto const advance = static_cast<std::size_t>(it - (str.begin() + begin));
    if (errno != 0 || advance <= 0) {
        errno = 0;
        return ParsingError { "Failed to parse int", str, begin, 0 };
    }
    begin += advance;
    return res;
}

[[nodiscard]] inline auto consumeDouble(std::string_view const & str, std::size_t & begin)
        -> std::variant<Double, ParsingError> {
    errno = 0;
    auto it = static_cast<char const *>(str.begin());
    auto const res = std::strtod(str.begin() + begin, const_cast<char **>(&it));
    auto const advance = static_cast<std::size_t>(it - (str.begin() + begin));
    if (errno != 0 || advance <= 0) {
        errno = 0;
        return ParsingError { "Failed to parse double", str, begin, 0 };
    }
    begin += advance;
    return res;
}

[[nodiscard]] inline auto consumeString(std::string_view const & str, std::size_t & begin)
        -> std::variant<String, ParsingError> {
    if (str.size() <= begin)
        return ParsingError { "Unexpected end of line", str, begin, 0 };

    auto i = begin;
    if (str[begin] == '"') {
        ++i;
        auto is_escaped = false;

        while (true) {
            if (i == str.size())
                return ParsingError { "Unexpected end of line while reading string",
                                      str,
                                      begin,
                                      i - begin };
            auto const c = str[i];
            if (c == '\\' && !is_escaped) {
                is_escaped = true;
            } else if (c == '"' && !is_escaped) {
                auto const start = begin;
                begin = i + 1;
                return String(&str[start + 1], &str[begin - 1]);
            } else {
                is_escaped = false;
                ++i;
            }
        }
    } else {
        auto is_escaped = false;

        while (true) {
            if (i == str.size() || (isWhiteSpace(str[i]) && !is_escaped)) {
                auto const start = begin;
                begin = i;
                return String(&str[start], &str[begin]);
            } else if (str[i] == '\\' && !is_escaped) {
                is_escaped = true;
            } else {
                is_escaped = false;
                ++i;
            }
        }
    }
}

}  // namespace eta
