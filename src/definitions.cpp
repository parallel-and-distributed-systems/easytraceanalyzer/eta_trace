#include "definitions.hpp"

#include <cstring>
#include <eta/core/format.hpp>
#include <nlc/meta/overloaded.hpp>
#include <nlc/meta/units.hpp>
#include <type_traits>

namespace eta {

auto to_string(ValueType v) -> std::string_view {
    switch (v) {
        case ValueType::Color: return "Color";
        case ValueType::Date: return "Date";
        case ValueType::Double: return "Double";
        case ValueType::Int: return "Int";
        case ValueType::Uint32: return "Uint32";
        case ValueType::Uint64: return "Uint64";
        case ValueType::String: return "String";
        case ValueType::StringView: return "StringView";
        case ValueType::EventId: return "EventId";
        case ValueType::ContainerId: return "ContainerId";
        case ValueType::CommunicationId: return "CommunicationId";
        default:
            Assert(false,
                   "Unknown eta::ValueType {}",
                   static_cast<typename std::underlying_type<ValueType>::type>(v));
            return "";
    }
}

auto operator<<(std::ostream & os, ValueType const & v) -> std::ostream & {
    return os << to_string(v);
}

auto to_string(Color const & color, ColorFormat format) -> std::string {
    auto toHex = [](char unsigned const i) -> char {
        if (i < 10)
            return '0' + i;
        else
            return 'A' + (i - 10);
    };
    switch (format) {
        case ColorFormat::HTML:
            return eta::format('#',
                               toHex(color.r / 16),
                               toHex(color.r % 16),
                               toHex(color.g / 16),
                               toHex(color.g % 16),
                               toHex(color.b / 16),
                               toHex(color.b % 16));
        case ColorFormat::Paje:
            return eta::format('"',
                               static_cast<double>(color.r) / 255.,
                               ' ',
                               static_cast<double>(color.g) / 255.,
                               ' ',
                               static_cast<double>(color.b) / 255.,
                               '"');
        case ColorFormat::RGB:
            return eta::format("rgb(",
                               static_cast<int>(color.r),
                               ", ",
                               static_cast<int>(color.g),
                               ", ",
                               static_cast<int>(color.b),
                               ')');
        case ColorFormat::RGB_d:
            return eta::format("rgb({})",
                               static_cast<double>(color.r) / 255.,
                               ", ",
                               static_cast<double>(color.g) / 255.,
                               ", ",
                               static_cast<double>(color.b) / 255.,
                               ')');
        default: return "UNKNOWN";
    }
}

auto to_string(Value const & v) -> std::string {
    return std::visit(
            nlc::meta::overloaded {
                    [](Int const & i) -> std::string { return std::to_string(i); },
                    [](Uint32 const & i) -> std::string { return std::to_string(i); },
                    [](Uint64 const & i) -> std::string { return std::to_string(i); },
                    [](Date const & d) -> std::string { return std::to_string(nlc::rm_unit(d)); },
                    [](Color const & c) -> std::string { return to_string(c); },
                    [](Double const & d) -> std::string { return std::to_string(d); },
                    [](String const & s) -> std::string { return eta::format('"', s, '"'); },
                    [](StringView const & s) -> std::string { return eta::format('"', s, '"'); },
                    [](EventId const & id) -> std::string {
                        return eta::format("Event(", id.value(), ')');
                    },
                    [](ContainerId const & id) -> std::string {
                        return eta::format("Container(", id.value(), ')');
                    },
                    [](CommunicationId const & id) -> std::string {
                        return eta::format("Communication(", id.value(), ')');
                    } },
            v);
}

auto operator<(Value const & lhs, Value const & rhs) -> bool {
    if (lhs.index() != rhs.index())
        return lhs.index() < rhs.index();

    return std::visit(
            nlc::meta::overloaded {
                    [](Color const & l, Color const & r) {
                        if (l.r == r.r) {
                            if (l.g == r.g) {
                                if (l.b == r.b) {
                                    return false;
                                } else
                                    return l.b < r.b;
                            } else
                                return l.g < r.g;
                        } else
                            return l.r < r.r;
                    },
                    [](Date const & l, Date const & r) {
                        return nlc::rm_unit(l) < nlc::rm_unit(r);
                    },
                    [](Double const & l, Double const & r) { return l < r; },
                    [](Int const & l, Int const & r) { return l < r; },
                    [](Uint32 const & l, Uint32 const & r) { return l < r; },
                    [](Uint64 const & l, Uint64 const & r) { return l < r; },
                    [](String const & l, String const & r) { return l < r; },
                    [](StringView const & l, StringView const & r) { return l < r; },
                    [](EventId const & l, EventId const & r) { return l < r; },
                    [](ContainerId const & l, ContainerId const & r) { return l < r; },
                    [](CommunicationId const & l, CommunicationId const & r) { return l < r; },
                    [](auto const &, auto const &) {
                        Assert(false, "");
                        return false;
                    } }  // namespace eta
            ,
            rhs,
            lhs);
}

auto operator<<(std::ostream & os, Color const & c) -> std::ostream & { return os << to_string(c); }

auto operator<<(std::ostream & os, Date const & d) -> std::ostream & {
    return os << nlc::rm_unit(d);
}

auto operator<<(std::ostream & os, EventId const & id) -> std::ostream & {
    return os << "Event(" << id.value() << ')';
}

auto operator<<(std::ostream & os, ContainerId const & id) -> std::ostream & {
    return os << "Container(" << id.value() << ')';
}

auto operator<<(std::ostream & os, CommunicationId const & id) -> std::ostream & {
    return os << "Communication(" << id.value() << ')';
}

auto operator<<(std::ostream & os, Value const & v) -> std::ostream & {
    std::visit(nlc::meta::overloaded { [&os](auto const & v) { os << v; } }, v);
    return os;
}

auto type_of(Value const & v) -> ValueType {
    switch (v.index()) {
        case 0: return ValueType::Color;
        case 1: return ValueType::Date;
        case 2: return ValueType::Double;
        case 3: return ValueType::Int;
        case 4: return ValueType::Uint32;
        case 5: return ValueType::Uint64;
        case 6: return ValueType::String;
        case 7: return ValueType::StringView;
        case 8: return ValueType::EventId;
        case 9: return ValueType::ContainerId;
        case 10: return ValueType::CommunicationId;
        default: Assert(false, ""); return static_cast<ValueType>(-1);
    }
}

auto to_string(EventType et) -> std::string_view {
    switch (et) {
        case EventType::CreateContainer: return "CreateContainer";
        case EventType::DestroyContainer: return "DestroyContainer";
        case EventType::SendMessage: return "SendMessage";
        case EventType::ReceiveMessage: return "ReceiveMessage";
        case EventType::EnterFunction: return "EnterFunction";
        case EventType::ExitFunction: return "ExitFunction";
        case EventType::IoOperationBegin: return "IoOperationBegin";
        case EventType::IoOperationComplete: return "IoOperationComplete";
        case EventType::MpiOperationBegin: return "MpiOperationBegin";
        case EventType::MpiOperationComplete: return "MpiOperationComplete";
        case EventType::Other: return "Other";
        default: Assert(false, "Unkwnon event type"); return "UNKWNOWN";
    }
}

auto operator<<(std::ostream & os, EventType et) -> std::ostream & { return os << to_string(et); }

auto FieldRegistry::register_new_field_name(std::string s) -> FieldId {
    Assert(get_field_id(s) == FieldId::invalid(), "Field \"", s, "\" already exists");
    auto const res = FieldId(static_cast<FieldId::underlying_t>(_field_names.size()));
    Assert(res.value() == _field_names.size() && res.isValid(),
           "Field count exceed FieldId capacity.");
    _field_names.emplace_back(std::move(s));
    return res;
}

auto FieldRegistry::get_field_name(FieldId id) const -> std::string const & {
    Assert(id.isValid() && id.value() < _field_names.size(),
           "{} && {} < {}",
           id.isValid(),
           id.value(),
           _field_names.size());
    return _field_names[id.value()];
}

auto FieldRegistry::get_field_id(std::string const & str) const -> FieldId {
    auto const end = _field_names.size();
    Assert(end < std::numeric_limits<FieldId::underlying_t>::max(),
           "Field count exceed FieldId capacity.");

    for (auto i = 0u; i < end; ++i) {
        if (_field_names[i] == str)
            return FieldId(static_cast<FieldId::underlying_t>(i));
    }
    return FieldId::invalid();
}

auto operator<<(std::ostream & os, std::optional<Value> const & v) -> std::ostream & {
    if (v.has_value())
        return os << v.value();
    return os;
}

auto get_field(Event const & event, FieldId id) -> std::optional<Value> {
    for (auto const & [field_id, value] : event.fields) {
        if (field_id == id)
            return value;
    }
    return std::nullopt;
}

auto to_string(CommunicationType c) -> std::string_view {
    switch (c) {
        case CommunicationType::P2P: return "P2P";
        case CommunicationType::Barrier: return "Barrier";
        case CommunicationType::BCast: return "BCast";
        case CommunicationType::Gather: return "Gather";
        case CommunicationType::Scatter: return "Scatter";
        case CommunicationType::Reduce: return "Reduce";
        case CommunicationType::Alltoall: return "Alltoall";
        case CommunicationType::Allgather: return "Allgather";
        case CommunicationType::Reduce_scatter: return "Reduce_scatter";
        case CommunicationType::Allreduce: return "Allreduce";
        case CommunicationType::Ibarrier: return "Ibarrier";
        case CommunicationType::Ibcast: return "Ibcast";
        case CommunicationType::Igather: return "Igather";
        case CommunicationType::Iscatter: return "Iscatter";
        case CommunicationType::Ireduce: return "Ireduce";
        case CommunicationType::Ialltoall: return "Ialltoall";
        case CommunicationType::Iallgather: return "Iallgather";
        case CommunicationType::Ireduce_scatter: return "Ireduce_scatter";
        case CommunicationType::Iallreduce: return "Iallreduce";
        case CommunicationType::OMP: return "OpenMP";
        case CommunicationType::Other: return "Other";
        default:
            Assert(false,
                   "Unknown eta::CommunicationType {}",
                   static_cast<typename std::underlying_type<CommunicationType>::type>(c));
            return "";
    }
}

auto operator<<(std::ostream & os, CommunicationType const & c) -> std::ostream & {
    return os << to_string(c);
}

struct Trace::TraceInitializerData {
    StandardFieldIds field_ids;
    FieldRegistry fields;

    TraceInitializerData() {
#define IMPL(name) field_ids.name = fields.register_new_field_name(#name)
        IMPL(CommunicationDestination);
        IMPL(CommunicationId);
        IMPL(CommunicationLength);
        IMPL(CommunicationSource);
        IMPL(CommunicationTag);
        IMPL(Container);
        IMPL(EnterFunctionEvent);
        IMPL(ExitFunctionEvent);
        IMPL(FunctionName);
#undef IMPL
    }
};

Trace::Trace() : Trace(TraceInitializerData()) {}

Trace::Trace(TraceInitializerData && data)
        : field_ids(std::move(data.field_ids))
        , fields(std::move(data.fields)) {}

auto Trace::createContainer(std::string container_name, ContainerId parentId, bool is_terminal)
        -> Container & {
    auto const container_id_value = static_cast<ContainerId::underlying_t>(containers.size());
    Assert(container_id_value == containers.size(), "Container count excede ContainerId capacity.");
    auto const container_id = ContainerId(container_id_value);
    containers.emplace_back();
    auto const container = &containers[container_id.value()];
    container->id = container_id;
    container->name = container_name;
    container->parent = parentId;
    container->is_terminal = is_terminal;
    container->offset = std::numeric_limits<double>::infinity();
    return *container;
}

auto Trace::push_back_event(Event & event, ContainerId container_id) -> void {
    if (event.id.isInvalid()) {
        event.id = EventId(events.size());
    }

    events.push_back(std::move(event));

    if (container_id.isValid()) {
        event.container_id = container_id;
    }

    if (event.container_id.isValid()) {
        container(event.container_id).events.push_back(event.id);
    } else {
        events_without_container.push_back(event.id);
    }
}

// ----------------------------------------------------

#ifdef ENABLE_OTF2
auto to_string(OTF2_CollectiveOp c) -> std::string {
    switch (c) {
        case OTF2_COLLECTIVE_OP_BARRIER: return "BARRIER";
        case OTF2_COLLECTIVE_OP_BCAST: return "BCAST";
        case OTF2_COLLECTIVE_OP_GATHER: return "GATHER";
        case OTF2_COLLECTIVE_OP_GATHERV: return "GATHERV";
        case OTF2_COLLECTIVE_OP_SCATTER: return "SCATTER";
        case OTF2_COLLECTIVE_OP_SCATTERV: return "SCATTERV";
        case OTF2_COLLECTIVE_OP_ALLGATHER: return "ALLGATHER";
        case OTF2_COLLECTIVE_OP_ALLGATHERV: return "ALLGATHERV";
        case OTF2_COLLECTIVE_OP_ALLTOALL: return "ALLTOALL";
        case OTF2_COLLECTIVE_OP_ALLTOALLV: return "ALLTOALLV";
        case OTF2_COLLECTIVE_OP_ALLTOALLW: return "ALLTOALLW";
        case OTF2_COLLECTIVE_OP_ALLREDUCE: return "ALLREDUCE";
        case OTF2_COLLECTIVE_OP_REDUCE: return "REDUCE";
        case OTF2_COLLECTIVE_OP_REDUCE_SCATTER: return "REDUCE_SCATTER";
        case OTF2_COLLECTIVE_OP_SCAN: return "SCAN";
        case OTF2_COLLECTIVE_OP_EXSCAN: return "EXSCAN";
        case OTF2_COLLECTIVE_OP_REDUCE_SCATTER_BLOCK: return "REDUCE_SCATTER_BLOCK";
        case OTF2_COLLECTIVE_OP_CREATE_HANDLE: return "CREATE_HANDLE";
        case OTF2_COLLECTIVE_OP_DESTROY_HANDLE: return "DESTROY_HANDLE";
        case OTF2_COLLECTIVE_OP_ALLOCATE: return "ALLOCATE";
        case OTF2_COLLECTIVE_OP_DEALLOCATE: return "DEALLOCATE";
        case OTF2_COLLECTIVE_OP_CREATE_HANDLE_AND_ALLOCATE: return "CREATE_HANDLE_AND_ALLOCATE";
        case OTF2_COLLECTIVE_OP_DESTROY_HANDLE_AND_DEALLOCATE:
            return "DESTROY_HANDLE_AND_DEALLOCATE";
        default: return "";
    }
}

auto to_communication_type(OTF2_CollectiveOp c) -> CommunicationType {
    switch (c) {
        case OTF2_COLLECTIVE_OP_BARRIER: return CommunicationType::Barrier;
        case OTF2_COLLECTIVE_OP_BCAST: return CommunicationType::BCast;
        case OTF2_COLLECTIVE_OP_GATHER: return CommunicationType::Gather;
        case OTF2_COLLECTIVE_OP_SCATTER: return CommunicationType::Scatter;
        case OTF2_COLLECTIVE_OP_REDUCE: return CommunicationType::Reduce;
        case OTF2_COLLECTIVE_OP_ALLTOALL: return CommunicationType::Alltoall;
        case OTF2_COLLECTIVE_OP_ALLGATHER: return CommunicationType::Allgather;
        case OTF2_COLLECTIVE_OP_REDUCE_SCATTER: return CommunicationType::Reduce_scatter;
        case OTF2_COLLECTIVE_OP_ALLREDUCE: return CommunicationType::Allreduce;
        default: return CommunicationType::Other;
    }
}
#endif

}  // namespace eta
